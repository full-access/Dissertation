#pragma once
#ifndef IRSET_HPP
#define IRSET_HPP

#include <iostream>
#include <set>

class IRSet {
private:
	std::set<std::string> _register;
public:
	IRSet(){}
	bool exists(std::string s) {
		if (_register.find(s) == _register.end()) {
			_register.insert(s);
			return 0;
		}
		return 1;
	}
};

#endif