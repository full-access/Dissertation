#pragma once
#ifndef DATASETGENERATOR_HPP
#define DATASETGENERATOR_HPP

#include <iostream>
#include "SampleExtractor.h"
#include <memory>

class DatasetGenerator {
public:
	void generate_dataset(std::string outpath, std::shared_ptr<SampleExtractor> se, std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> programs) {
		std::shared_ptr<std::vector<float>> res = std::make_shared<std::vector<float>>();
		for (int i = 0; i < programs->size(); i++) {
			res = se->generate_samples(programs->at(0));
		}
		res->clear();
	}
};

#endif 