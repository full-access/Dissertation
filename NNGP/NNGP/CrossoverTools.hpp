#pragma once
#ifndef CROSSOVERTOOLS_HPP
#define CROSSOVERTOOLS_HPP

#include "SelectionSet.hpp"
#include "AbstractNode.hpp"
#include "StochasticMath.hpp"
#include <memory>
#include <algorithm> 
#include "NodeLogger.hpp"
#include "GPTools.hpp"

class CrossoverTools {
public:
	int island_id;

	CrossoverTools(int island_id) {
		this->island_id = island_id;
	}

	int crossover(std::shared_ptr<AbstractNode> a, std::shared_ptr<AbstractNode> b) {

		std::shared_ptr<SelectionSet> as = std::static_pointer_cast<ProgramHandle>(a)->selset;
		std::shared_ptr<SelectionSet> bs = std::static_pointer_cast<ProgramHandle>(b)->selset;

		std::shared_ptr<NodeLogger> nl = std::make_shared<NodeLogger>();

		if ((as->vecs->size() == 0 && bs->floats->size() == 0) || (as->floats->size() == 0 && bs->vecs->size() == 0)) {
			return -1;
		}

		// should we use floats or vecs?
		bool selfloats = false;
		if (as->floats->size() == 0 || bs->floats->size() == 0) {
			selfloats = false;
		}else {
			if (as->vecs->size() == 0 || bs->vecs->size() == 0) {
				selfloats = true;
			}else {
				selfloats = StochasticMath().selprop({ (float)(as->floats->size() + bs->floats->size()), (float)(as->vecs->size() + bs->vecs->size()) });
			}
		}
		
		// selecting the appropriate nodes using randint
		std::set<std::shared_ptr<AbstractNode>>::const_iterator at;
		std::set<std::shared_ptr<AbstractNode>>::const_iterator bt;
		if (selfloats) {
			do{
				at = as->floats->begin();
				advance(at, StochasticMath().randint(0, as->floats->size() - 1));
			} while (at == as->floats->end());
			do{
				bt = bs->floats->begin();
				advance(bt, StochasticMath().randint(0, bs->floats->size() - 1));
			} while (bt == bs->floats->end());
		}
		else {
			at = as->vecs->begin();
			advance(at, StochasticMath().randint(0, as->vecs->size() - 1));
			bt = bs->vecs->begin();
			advance(bt, StochasticMath().randint(0, bs->vecs->size() - 1));
		}

		std::shared_ptr<AbstractNode> anode = *at;
		std::shared_ptr<AbstractNode> bnode = *bt;

		anode->accept(nl);
		std::shared_ptr<SelectionSet> anode_ss = nl->get_and_delete();
		bnode->accept(nl);
		std::shared_ptr<SelectionSet> bnode_ss = nl->get_and_delete();

		GPTools(island_id).difference(as, anode_ss);
		GPTools(island_id).difference(bs, bnode_ss);
		GPTools(island_id).unite(as, bnode_ss);
		GPTools(island_id).unite(bs, anode_ss);

		// extract the parents and exchange them
		std::shared_ptr<AbstractNode> temp;
		int aindex = anode->index_as_parameter;
		int bindex = bnode->index_as_parameter;

		if (anode->parent.lock()->parent.lock() != nullptr) {
			std::shared_ptr<GPNonterminalWrapper> parent = std::static_pointer_cast<GPNonterminalWrapper>(anode->parent.lock());
			temp = parent;
			parent->t->parameters->at(aindex) = bnode;
			bnode->set_index(aindex);
		}else{
			std::shared_ptr<ProgramHandle> parent = std::static_pointer_cast<ProgramHandle>(anode->parent.lock());
			temp = parent;
			parent->child = bnode;
			bnode->set_index(0);
		}
		anode->set_parent(bnode->parent.lock());
		if (bnode->parent.lock()->parent.lock() != nullptr) {
			std::shared_ptr<GPNonterminalWrapper> parent = std::static_pointer_cast<GPNonterminalWrapper>(bnode->parent.lock());
			parent->t->parameters->at(bindex) = anode;
			anode->set_index(bindex);
		}else {
			std::shared_ptr<ProgramHandle> parent = std::static_pointer_cast<ProgramHandle>(bnode->parent.lock());
			parent->child = anode;
			anode->set_index(0);
		}
		bnode->set_parent(temp);
	}
};

#endif