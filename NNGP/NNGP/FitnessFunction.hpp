#pragma once
#ifndef FITNESSFUNCTION_HPP
#define FITNESSFUNCTION_HPP

#include <memory>
#include <vector>
#include "AbstractNode.hpp"
#include "AbstractDatum.hpp"
#include "Datum.hpp"
#include "FileIO.hpp"
#include "StochasticMath.hpp"
#include <limits.h>
#include "BRDFReader.hpp"

class FitnessFunction {
public:

	float attenuation;
	float max_attenuation;
	std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> inputs;
	std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> out;
	std::shared_ptr<std::vector<bool>> format;
	BrdfReader brdfr;
	double* brdf;

	FitnessFunction(std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> inputs) {
		if (!brdfr.read_brdf("C:\\Users\\James-MSI\\Desktop\\gold-metallic-paint.binary", brdf)) {
			fprintf(stderr, "Error reading \n");
			exit(1);
		}
		this->inputs = inputs;
		max_attenuation = 5;
		attenuation = max_attenuation;
	}

	float mdot(glm::vec3 a, glm::vec3 b) {
		return std::fmax(0, glm::dot(a, b));
	}

	//                                  PHONG
	//std::shared_ptr<AbstractDatum> groundtruth(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> arg_list) {
	//std::shared_ptr<std::vector<glm::vec3>> ins = std::make_shared<std::vector<glm::vec3>>();
	//for (int i = 0; i < arg_list->size(); i++) {
	//ins->push_back((*std::static_pointer_cast<glm::vec3>(arg_list->at(i)->value())));
	//}
	//glm::vec3 L = ins->at(0);
	//glm::vec3 V = ins->at(1);
	//glm::vec3 N = ins->at(2);
	//glm::vec3 H = ins->at(3);
	//glm::vec3 R = ins->at(4);

	//float kd = 2.0;
	//float ks = 0.3;
	//float n = 3.0;
	//float pi = 3.14159265358979323846264338;

	//float brdf = kd * (1.0 / pi) + ks * (((n + 2.0) / (2.0*pi))*pow(cos(dot(R, V)), n));

	//std::shared_ptr<Datum<float>> ret = std::make_shared<Datum<float>>(brdf);
	//return ret;
	//}

	////                              COOK TORRANCE
	//std::shared_ptr<AbstractDatum> groundtruth(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> arg_list) {
	//	std::shared_ptr<std::vector<glm::vec3>> ins = std::make_shared<std::vector<glm::vec3>>();
	//	for (int i = 0; i < arg_list->size(); i++) {
	//		ins->push_back((*std::static_pointer_cast<glm::vec3>(arg_list->at(i)->value())));
	//	}
	//	glm::vec3 L = ins->at(0);
	//	glm::vec3 V = ins->at(1);
	//	glm::vec3 N = ins->at(2);
	//	glm::vec3 H = ins->at(3);
	//	glm::vec3 R = ins->at(4);

	//	//float r0 = 0.3;
	//	//float m = 0.3; // roughness
	//	//float kd = 0.75;
	//	//float ks = 0.2;

	//	float r0 = 0.3;
	//	float m = 0.15; // roughness
	//	float kd = 0.2;
	//	float ks = 0.8;

	//	float delta = std::acos(glm::dot(H, N));

	//	float D = std::exp((mdot(N, H)*mdot(N, H) - 1.0) / (m*m*mdot(N, H)*mdot(N, H))) / (m*m*std::pow(mdot(N, H), 4));
	//	
	//	float G_0 = 2.0*mdot(N, H)*mdot(N, V) / mdot(V, H);
	//	float G_1 = 2.0*mdot(N, H)*mdot(N, L) / mdot(V, H);
	//	float G = std::fmin(std::fmin(G_1, G_0), 1.0);

	//	float F = r0 + (1 - r0)*(1.0 - std::pow(mdot(V, H), 5));

	//	float Rs = (F * D * G) / (3.1415926 * mdot(N, L)* mdot(N, V));
	//	float ct = kd*mdot(N, L) + (Rs * mdot(N, L) * (1.0 - kd));

	//	std::shared_ptr<Datum<float>> ret = std::make_shared<Datum<float>>(ct);
	//	return ret;
	//}

	float Beckmann(float m, float t)
	{
		float M = m * m;
		float T = t * t;
		return std::exp((T - 1) / (M*T)) / (M*T*T);
	}

	//                              COOK TORRANCE 2
	std::shared_ptr<AbstractDatum> groundtruth(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> arg_list) {
		std::shared_ptr<std::vector<glm::vec3>> ins = std::make_shared<std::vector<glm::vec3>>();
		for (int i = 0; i < arg_list->size(); i++) {
			ins->push_back((*std::static_pointer_cast<glm::vec3>(arg_list->at(i)->value())));
		}
		glm::vec3 L = ins->at(0);
		glm::vec3 V = ins->at(1);
		glm::vec3 N = ins->at(2);
		glm::vec3 H = ins->at(3);
		//glm::vec3 actualH = glm::normalize(L + V);
		glm::vec3 R = ins->at(4);

		float r0 = 0.9;
		float m = 0.5; // roughness
		float kd = 0.8;
		float ks = 0.2;

		float D = Beckmann(m, glm::dot(N, H));
		float F = r0 + (1 - r0)*(std::pow(1.0 - glm::dot(V, H), 5));

		float G_0 = 2.0*glm::dot(N, H)*glm::dot(N, V) / glm::dot(V, H);
		float G_1 = 2.0*glm::dot(N, H)*glm::dot(N, L) / glm::dot(V, H);
		float G = std::fmin(std::fmin(G_1, G_0), 1.0);

		float val = D * G / glm::dot(N,L);
		val *= F / 3.14159265358979323846264338f;
		
		std::shared_ptr<Datum<float>> ret = std::make_shared<Datum<float>>(ks*val + kd/(3.14159265358979323846264338f));
		return ret;
	}

	//std::shared_ptr<AbstractDatum> groundtruth(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> arg_list) {
	//	std::shared_ptr<std::vector<glm::vec3>> ins = std::make_shared<std::vector<glm::vec3>>();
	//	for (int i = 0; i < 2; i++) {
	//		ins->push_back((*std::static_pointer_cast<glm::vec3>(arg_list->at(i)->value())));
	//	}
	//	glm::vec3 L = ins->at(0);
	//	glm::vec3 V = ins->at(1);

	//	float theta_in = std::acos(L.z);
	//	float phi_in = std::atan(L.y / (float)L.x);

	//	float theta_out = std::acos(V.z);
	//	float phi_out = std::atan(V.y / (float)V.x);

	//	double r, g, b;

	//	brdfr.lookup_brdf_val(brdf, theta_in, phi_in, theta_out, phi_out, r, g, b);

	//	std::shared_ptr<Datum<float>> ret = std::make_shared<Datum<float>>(b);
	//	return ret;
	//}

	void test_sample(std::shared_ptr<ProgramHandle> p, int num_tests) {
		StochasticMath sm;
		for (int i = 0; i < num_tests; i++) {
			int sel = sm.randint(0, inputs->size() - 1);
			std::shared_ptr<AbstractDatum> target = groundtruth(inputs->at(sel));
			float a = (*std::static_pointer_cast<float>(target->value()));
			float b = (*std::static_pointer_cast<float>(p->resolve(inputs->at(sel))));
			std::cout << a << "   :   " << b << std::endl;
		}
	}

	double float_accuracy(std::shared_ptr<AbstractDatum> _a, float _b) {
		float a = (*std::static_pointer_cast<float>(_a->value()));
		float b = _b;
		if ((std::isnan(a) || std::isinf(a)) && (std::isnan(b) || std::isinf(b))) {
			return 0;
		}
		if ((std::isnan(a) || std::isinf(a)) && !(std::isnan(b) || std::isinf(b))) {
			return std::numeric_limits<double>::max();
		}
		if (!(std::isnan(a) || std::isinf(a)) && (std::isnan(b) || std::isinf(b))) {
			return std::numeric_limits<double>::max();
		}

		//float tempa = a * 1000000;
		//float tempb = b * 1000000;
		//int ta = 0;
		//int tb = 0;
		//while ((tempa /= 10) > 1) ta++;
		//while ((tempb /= 10) > 1) tb++;
		//a = a * 1000000 - (std::pow(10, (ta)));
		//b = b * 1000000 - (std::pow(10, (tb)));
		//return std::pow((a - b), 2);

		return std::abs(a * 1000000 - b * 1000000);
	}

	double vec3_accuracy(std::shared_ptr<AbstractDatum> _a, glm::vec3 _b) {
		glm::vec3 a = (*std::static_pointer_cast<glm::vec3>(_a->value()));
		glm::vec3 b = _b;
		double accum = 0;
		for (int i = 0; i < 3; i++) {
			float ac = a[i];
			float bc = b[i];
			if (std::isnan(ac) || std::isinf(ac)) {
				if (std::isnan(bc) || std::isinf(bc)) {
					continue;
				}
				accum += 100000000000;
			}
			accum += std::abs(ac - bc);
		}
		return (accum / 3.0);
	}

	template <typename T>
	double fitness(std::shared_ptr<AbstractNode> program, int num_samples) {
		StochasticMath sm;
		double fit = 0;
		// calculate size
		std::shared_ptr<ProgramHandle> proghandle = std::static_pointer_cast<ProgramHandle>(program);
		int size = proghandle->selset->floats->size() + proghandle->selset->vecs->size();

		for (int i = 0; i < num_samples; i++) {

			int sel = sm.randint(0, inputs->size() - 1);

			std::shared_ptr<AbstractDatum> target = groundtruth(inputs->at(sel));
			if (std::is_same<T, float>::value) {
				float res = (*std::static_pointer_cast<float>(program->resolve(inputs->at(sel))));
				double acc = float_accuracy(target, res);
				if (std::isnan(acc)) {
					std::cout << "break";
				}
				if (std::isinf(acc)) {
					fit += 1e30;
				}
				if (acc == std::numeric_limits<double>::max()) {
					fit += 1e30;
				}
				else {
					fit += acc / (double)num_samples;
				}
			}
			if (std::is_same<T, glm::vec3>::value) {
				glm::vec3 res = (*std::static_pointer_cast<glm::vec3>(program->resolve(inputs->at(sel))));
				double acc = vec3_accuracy(target, res);
				fit += acc / (double)num_samples;
			}
		}
		double returnme = (pow(fit, attenuation) + size * size * 10000);
		return returnme;
	}

	double fl_mse(std::shared_ptr<AbstractDatum> _a, float _b) {
		float a = (*std::static_pointer_cast<float>(_a->value()));
		float b = _b;
		return (a - b)*(a - b);
	}

	template <typename T>
	double mse(std::shared_ptr<AbstractNode> program, int num_samples) {
		StochasticMath sm;
		double fit = 0;
		// calculate size
		std::shared_ptr<ProgramHandle> proghandle = std::static_pointer_cast<ProgramHandle>(program);
		int size = proghandle->selset->floats->size() + proghandle->selset->vecs->size();
		float mse = 0;
		for (int i = 0; i < num_samples; i++) {
			int sel = sm.randint(0, inputs->size() - 1);
			std::shared_ptr<AbstractDatum> target = groundtruth(inputs->at(sel));
			if (std::is_same<T, float>::value) {
				float res = (*std::static_pointer_cast<float>(program->resolve(inputs->at(sel))));
				mse += fl_mse(target, res) / num_samples;
			}
		}
		return mse;
	}

	~FitnessFunction() {
		delete brdf;
	}
};

#endif