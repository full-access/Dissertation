#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <string>
#include <cstring>

#include "GPNonterminalWrapper.hpp"
#include "GPBehaviourWrapper.hpp"
#include "GPTerminalWrapper.hpp"
#include "Type.hpp"
#include "AbstractNodeWrapper.hpp"
//
#include "Register.hpp"
//#include "RegisterAnalyser.hpp"
//#include "Printer.hpp"
//
//#include "GPProgram.hpp"
//#include "Datum.hpp"
//#include "AbstractDatum.hpp"
//#include "StochasticMath.hpp"
//
//#include <boost\any.hpp>
//#include <typeindex>
//#include "RHHSynthesizer.h"
#include "CppTranslator.hpp"
#include "FileIO.hpp"
//
//#include "IntermediateRepresentationTranslator.hpp"
//#include "GPIROptimiser.hpp"
//#include "GPIRFilter.hpp"
//#include "IRLexer.hpp"
//#include "IRParser.hpp"
#include "IRPopulationGenerator.hpp"
#include "IRFileExtractor.hpp"
#include "IRPopulationASTReconstructor.hpp"
//#include "InputGenerator.hpp"
#include "SampleExtractor.h"
//#include "NodeLogger.hpp"
//#include "CrossoverTools.hpp"
//#include "GPTools.hpp"
//#include "GPLauncher.hpp"
#include "IslandModel.hpp"
#include "BRDFReader.hpp"

#include "TextToArgumentParser.hpp"

template <typename T>
void extract_and_sample(std::string path, std::shared_ptr<std::vector<std::string>> format, std::string extract_from, std::string outfile, std::shared_ptr<std::vector<std::string>> infiles, std::string attfile, int projected_size, int num_training_samples) {

	std::shared_ptr<IRFileExtractor> irfe = std::make_shared<IRFileExtractor>(extract_from);
	std::shared_ptr<IRPopulationASTReconstructor> irpastr = std::make_shared<IRPopulationASTReconstructor>(format);
	SampleExtractor se(path);

	std::shared_ptr<FileIO> o_saver = std::make_shared<FileIO>(outfile);
	std::shared_ptr<std::vector<std::shared_ptr<FileIO>>> i_savers = std::make_shared<std::vector<std::shared_ptr<FileIO>>>();
	for (int i = 0; i < infiles->size(); i++) {
		i_savers->push_back(std::make_shared<FileIO>(infiles->at(i)));
	}
	FileIO av_saver(attfile);
	int count = 0;
	int totals = 0;

	std::shared_ptr<std::vector<std::shared_ptr<AttributeVector>>> avs;
	std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> progs;

	int end = 0;
	while (end == 0) {
		std::shared_ptr<std::vector<std::string>> irs = irfe->extract(10000, end);

		if (end == 1) {
			if (avs != nullptr && avs->size() > 0) {
				std::map<std::string, int>::iterator it = avs->at(0)->attrib_freqs->begin();
				std::cout << "Attribute vector format: " << std::endl;
				while (it != avs->at(0)->attrib_freqs->end()) {
					std::cout << it->first << "   ";
					it++;
				}
				std::cout << std::endl;
			}
		}

		progs = irpastr->reconstruct(irs);
		avs = irpastr->get_and_delete_attrib_vecs();

		totals += progs->size();
		std::cout << "Extracted " + std::to_string(progs->size()) + " programs ("+std::to_string(totals/(float)(projected_size))+"% of projected program amount)." << std::endl;

		for (int i = 0; i < progs->size(); i++) {
			if (i % ((int)(progs->size() / (float)1000) + 1) == 0) {
				std::cout << std::to_string(i) + " / " + std::to_string(progs->size()) + " (" + std::to_string(((i) / (float)progs->size()) * 100) + "%) programs sampled.\r" << std::flush;
			}
			int avs_count = se.generate_samples<T>(path, progs->at(i), o_saver, i_savers, 100, 5, num_training_samples);
			if (avs_count > 0) {
				count++;
				for (int j = 0; j < avs_count; j++)
					av_saver.save_attrib_vector(avs->at(i));
			}
		}
	}
	std::cout << count << "Programs processed" << std::endl;
}


int point(int current, int maxdepth, int count) {
	int sum = (maxdepth / (float)2)*(1 + maxdepth);
	return (int)(count * (current / (float)sum));
}

//float x1(float x) {
//	float ret = (((((float(3.141593) + float(3.141593)) * float(2.000000)) * x) + (x * x)) + ((float(3.141593) * float(3.141593)) * (((x * x) + (((float(3.141593) + float(3.141593)) * float(2.000000)) + float(2.000000))) * float(3.141593))));
//	return ret;
//}
//
//float x2(float x) {
//	return (32 * x*x) + (12.5*x) + 451;
//}
//
//int main(void) {
//
//	//std::cout <<"           \tCONTROL\t\tEVOLVED" << std::endl;
//	//for (int i = -25; i < 25; i++) {
//	//	std::cout << "Input: " << i<< "\t" << x2(i) << "\t\t" << x1(i) << std::endl;
//	//}
//	//return 0;
//
//	//float ret = ((float(3.1415925) + float(3.1415925)) * (std::sqrt(float(2.000000)) / std::sqrt(std::sqrt(std::sin(float(3.1415925))))));
//	//std::cout << ret << std::endl;
//	//return 0;
//
//	std::shared_ptr<GPBehaviourWrapper<float>> radd = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX+", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
//	radd->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) + *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
//		return std::static_pointer_cast<void>(ret); 
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> rsub = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX-", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
//	rsub->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) - *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
//		return std::static_pointer_cast<void>(ret);
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> rmult = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX*", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
//	rmult->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) * *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
//		return std::static_pointer_cast<void>(ret);
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> rdiv = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX/", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
//	rdiv->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) / *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
//		return std::static_pointer_cast<void>(ret);
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> rsqrt = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::sqrt", { std::make_shared<Type<float>>()}));
//	rsqrt->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(std::sqrt(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
//		return std::static_pointer_cast<void>(ret);
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> rsin = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::sin", { std::make_shared<Type<float>>() }));
//	rsin->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(std::sin(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
//		return std::static_pointer_cast<void>(ret);
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> rcos = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::cos", { std::make_shared<Type<float>>() }));
//	rcos->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(std::cos(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
//		return std::static_pointer_cast<void>(ret);
//	};
//
//	std::shared_ptr<GPBehaviourWrapper<glm::vec3>> vcross = std::shared_ptr<GPBehaviourWrapper<glm::vec3>>(new GPBehaviourWrapper<glm::vec3>(1, "glm::cross", { std::make_shared<Type<glm::vec3>>(), std::make_shared<Type<glm::vec3>>() }));
//	vcross->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<glm::vec3> ret = std::make_shared<glm::vec3>(glm::cross(*(std::static_pointer_cast<glm::vec3>(a->at(0)->resolve(d))), *(std::static_pointer_cast<glm::vec3>(a->at(1)->resolve(d)))));
//		return std::static_pointer_cast<void>(ret); 
//	};
//	std::shared_ptr<GPBehaviourWrapper<float>> vdot = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "glm::dot", { std::make_shared<Type<glm::vec3>>(), std::make_shared<Type<glm::vec3>>() }));
//	vdot->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
//		std::shared_ptr<float> ret = std::make_shared<float>(glm::dot(*(std::static_pointer_cast<glm::vec3>(a->at(0)->resolve(d))), *(std::static_pointer_cast<glm::vec3>(a->at(1)->resolve(d)))));
//		return std::static_pointer_cast<void>(ret);
//	};
//
//
//	//Register<glm::vec3>::getinstance()->log(vcross);
//	Register<float>::getinstance()->log(vdot, 1);
//	//Register<glm::vec3>::getinstance()->log(glm::vec3(2, 5, 5), true);
//	//Register<glm::vec3>::getinstance()->log(glm::vec3(3, 3, 3), true);
//
//	//Register<float>::getinstance()->log(radd, 0.44369158);
//	//Register<float>::getinstance()->log(rsub, 0.54520494);
//	//Register<float>::getinstance()->log(rmult, 0.45568672);
//	//Register<float>::getinstance()->log(rdiv, 0.36447552);
//	//Register<float>::getinstance()->log(rsqrt, 0.34171173);
//	//Register<float>::getinstance()->log(rsin, 0.4243745);
//	//Register<float>::getinstance()->log(rcos, 0.44662386);
//	Register<float>::getinstance()->log(radd, 1);
//	Register<float>::getinstance()->log(rsub, 1);
//	Register<float>::getinstance()->log(rmult, 1);
//	Register<float>::getinstance()->log(rdiv, 1);
//	Register<float>::getinstance()->log(rsqrt, 1);
//	Register<float>::getinstance()->log(rsin, 1);
//	Register<float>::getinstance()->log(rcos, 1);
//	Register<float>::getinstance()->log(1.0f, true, 0);
//	//Register<float>::getinstance()->log(2.0f, true);
//	//Register<float>::getinstance()->log(3.1415926f, true);
//	//Register<float>::getinstance()->log(NAN, false);
//	Register<glm::vec3>::getinstance()->log(glm::vec3(NAN), false, 0);
//
//	std::shared_ptr<std::vector<std::string>> format = std::make_shared<std::vector<std::string>>();
//	format->push_back("INFIX+");
//	format->push_back("INFIX-");
//	format->push_back("INFIX*");
//	format->push_back("INFIX/");
//	format->push_back("std::sqrt");
//	format->push_back("std::sin");
//	format->push_back("std::cos");
//	format->push_back("glm::dot");
//	//format->push_back("depth");
//
//
//	std::shared_ptr<IRPopulationGenerator> irpg = std::make_shared<IRPopulationGenerator>();
//
//	int samples_per_program = 4;
//	int program_count = 100;
//	int maxdepth = 3;
//
//	std::string ext = "_0";
//
//	irpg->generate_dataset<float>(maxdepth, program_count, "G:\\", "vec3_irs" + ext, 0.01, false, false);
//	//irpg->generate_dataset(maxdepth, program_count, "G:\\float_irs"+ext+".txt", false);
//
//	for (int i = 0; i < maxdepth + 1; i++) {
//		const std::string& ss = ("G:\\vec3_irs" + ext + std::to_string(i) + ".txt");
//		const char * s = ss.c_str();
//		std::remove(s);
//	}
//
//	extract_and_sample(format, "G:\\vec3_irs"+ext+".txt", "G:\\vec3_os"+ext+".csv", "G:\\vec3_is" + ext+".csv", "G:\\vec3_at" + ext + ".csv", program_count);
//	std::cout << "Extracting Validation sets:" << std::endl;
//	for (int i = 1; i < maxdepth+1; i++) {
//		extract_and_sample(format, "G:\\vec3_irs" + ext +std::to_string(i)+"_val.txt", "G:\\vec3_os" + ext + std::to_string(i)+"_val.csv", "G:\\vec3_is" + ext + std::to_string(i)+"_val.csv", "G:\\vec3_at" + ext + std::to_string(i)+"_val.csv", 100000);
//	}
//
//	////float ret = (float(3.141593) * (((float(2.000000) * float(3.141593)) * (float(3.141593) * (float(2.000000) * (float(2.000000) * float(2.000000))))) * std::sin(float(2.000000))));
//	////std::cout << ret << std::endl;
//
//	//std::shared_ptr<std::vector<std::shared_ptr<GPLauncher>>> threads = std::make_shared<std::vector<std::shared_ptr<GPLauncher>>>();
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//threads->push_back(std::make_shared<GPLauncher>(true));
//	//float migration_rate = 0.005;
//	//IslandModel isl(threads, migration_rate, "C:\\Users\\James-MSI\\Desktop\\perf_nn_5.csv", "C:\\Users\\James-MSI\\Desktop\\irs_nn_5.csv");
//	//std::shared_ptr<CommonPool> pool = isl.pool;
//	//
//	//std::shared_ptr<CppTranslator> cpp = std::make_shared<CppTranslator>("float");
//	//std::pair<std::shared_ptr<ProgramHandle>, double> pf;
//	//int min = -1;
//	//std::shared_ptr<ProgramHandle> best;
//	//while ((pf = pool->pop()).first != nullptr) {
//	//	if (min == -1 || min > pf.second) {
//	//		min = pf.second;
//	//		best = pf.first;
//	//	}
//	//}
//	//best->accept(cpp);
//	//std::cout << cpp->get_and_delete() << std::endl;
//	//std::cout << "FIT: " << min << std::endl << std::endl;
//	return 0;
//}

float evolved2(glm::vec3 V, glm::vec3 R) {
	float ret = (std::sin(1.000000) / (std::fmin(4.000000, ((std::tan(std::tan(glm::dot(V, R))) * (glm::dot(V, R) / (std::tan(13.000000) / 3.141593))) - 1.000000)) + std::sqrt(std::sqrt(4.000000))));
	return ret;
}

float evolved(glm::vec3 V, glm::vec3 R) {
	float ret = (0.841471 / (std::fmin(4.000000, ((std::tan(std::tan(glm::dot(V, R))) * (glm::dot(V, R) / 0.147384)) - 1.000000)) + 1.414214));
	return ret;
}

float clamp(float inp) {
	if (inp < 0) return 0;
	if (inp > 1) return 1;
	return inp;
}

int main(void) {

	/*FileIO fio;
	auto inputs = fio.load("C:\\Users\\James-MSI\\Desktop\\vecs_dataset.csv");

	for (int i = 0; i < 100; i++) {

		glm::vec3 argv[6];
		argv[0] = glm::vec3(1, 1, 1);
		argv[1] = *std::static_pointer_cast<glm::vec3>(inputs->at(i)->at(0)->value());
		argv[2] = *std::static_pointer_cast<glm::vec3>(inputs->at(i)->at(1)->value());
		argv[3] = *std::static_pointer_cast<glm::vec3>(inputs->at(i)->at(2)->value());
		argv[4] = *std::static_pointer_cast<glm::vec3>(inputs->at(i)->at(3)->value());
		argv[5] = *std::static_pointer_cast<glm::vec3>(inputs->at(i)->at(4)->value());

		float ret = std::fmax(glm::dot(argv[2], argv[4]), std::tan(glm::dot(argv[4], argv[1])));

		float highlight = 5;
		float specular = std::pow(glm::dot(argv[2], argv[5]), highlight);
		float diffuse = glm::dot(argv[3], argv[1]);

		std::cout << ret << "        " << diffuse + specular << std::endl;
	}

	return 0;
*/
	std::shared_ptr<GPBehaviourWrapper<float>> radd = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX+", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	radd->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) + *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
		return std::static_pointer_cast<void>(ret); 
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rsub = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX-", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	rsub->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) - *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rmult = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX*", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	rmult->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) * *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rdiv = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "INFIX/", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	rdiv->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))) / *(std::static_pointer_cast<float>(a->at(1)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rsqrt = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::sqrt", { std::make_shared<Type<float>>()}));
	rsqrt->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::sqrt(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rclamp = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "clamp", { std::make_shared<Type<float>>() }));
	rclamp->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(clamp(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rsin = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::sin", { std::make_shared<Type<float>>() }));
	rsin->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::sin(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rcos = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::cos", { std::make_shared<Type<float>>() }));
	rcos->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::cos(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rtan = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::tan", { std::make_shared<Type<float>>() }));
	rtan->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::tan(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rexp = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::exp", { std::make_shared<Type<float>>() }));
	rexp->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::exp(*std::static_pointer_cast<float>(a->at(0)->resolve(d))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rmin = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::fmin", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	rmin->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::fmin(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))), *(std::static_pointer_cast<float>(a->at(1)->resolve(d)))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rmax = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::fmax", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	rmax->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::fmax(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))), *(std::static_pointer_cast<float>(a->at(1)->resolve(d)))));
		return std::static_pointer_cast<void>(ret);
	};
	std::shared_ptr<GPBehaviourWrapper<float>> rpow = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "std::pow", { std::make_shared<Type<float>>(), std::make_shared<Type<float>>() }));
	rpow->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(std::pow(*(std::static_pointer_cast<float>(a->at(0)->resolve(d))), *(std::static_pointer_cast<float>(a->at(1)->resolve(d)))));
		return std::static_pointer_cast<void>(ret);
	};
	
	std::shared_ptr<GPBehaviourWrapper<glm::vec3>> vcross = std::shared_ptr<GPBehaviourWrapper<glm::vec3>>(new GPBehaviourWrapper<glm::vec3>(1, "glm::cross", { std::make_shared<Type<glm::vec3>>(), std::make_shared<Type<glm::vec3>>() }));
	vcross->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<glm::vec3> ret = std::make_shared<glm::vec3>(glm::cross(*(std::static_pointer_cast<glm::vec3>(a->at(0)->resolve(d))), *(std::static_pointer_cast<glm::vec3>(a->at(1)->resolve(d)))));
		return std::static_pointer_cast<void>(ret); 
	};
	std::shared_ptr<GPBehaviourWrapper<float>> vdot = std::shared_ptr<GPBehaviourWrapper<float>>(new GPBehaviourWrapper<float>(0, "glm::dot", { std::make_shared<Type<glm::vec3>>(), std::make_shared<Type<glm::vec3>>() }));
	vdot->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>& d) {
		std::shared_ptr<float> ret = std::make_shared<float>(glm::dot(*(std::static_pointer_cast<glm::vec3>(a->at(0)->resolve(d))), *(std::static_pointer_cast<glm::vec3>(a->at(1)->resolve(d)))));
		return std::static_pointer_cast<void>(ret);
	};

	Register<float>::getinstance()->log(rmult);
	Register<float>::getinstance()->log(radd);
	Register<float>::getinstance()->log(rsub);
	Register<float>::getinstance()->log(rdiv);
	Register<float>::getinstance()->log(rclamp);
	Register<float>::getinstance()->log(vdot);
	Register<float>::getinstance()->log(rcos);
	Register<float>::getinstance()->log(rexp);
	Register<float>::getinstance()->log(rmax);
	Register<float>::getinstance()->log(rmin);
	Register<float>::getinstance()->log(rpow);
	Register<float>::getinstance()->log(rsin);
	Register<float>::getinstance()->log(rsqrt);	
	Register<float>::getinstance()->log(rtan);

	Register<float>::getinstance()->add_dist({ 0.5242707, 0.40517455, 0.5151539, 0.4422373, 0.2571978, 0.27547258, 0.46475822, 0.63175815, 0.3209157, 0.2824908, 0.88630927, 0.4489174, 0.71058476, 0.4094326 });
	Register<float>::getinstance()->add_dist({ 0.5242707, 0.40517455, 0.5151539, 0.4422373, 0.2571978, 0.27547258, 0.46475822, 0.63175815, 0.3209157, 0.2824908, 0.88630927, 0.4489174, 0.71058476, 0.4094326 });
	Register<float>::getinstance()->add_dist({ 0.8753753, 0.7257284, 0.92763156, 0.7244096, 0.49128342, 0.5244903, 0.69874257, 0.85582983, 0.60110223, 0.55275714, 0.99932086, 0.6883298, 0.9502803, 0.6488415 });
	Register<float>::getinstance()->add_dist({ 0.8753753, 0.7257284, 0.92763156, 0.7244096, 0.49128342, 0.5244903, 0.69874257, 0.85582983, 0.60110223, 0.55275714, 0.99932086, 0.6883298, 0.9502803, 0.6488415 });
	Register<float>::getinstance()->add_dist({ 1.0, 1.0, 1.0, 1.0, 0.868632, 0.7391328, 0.99856883, 1.0, 0.94258595, 0.9154827, 0.9999925, 0.99877995, 0.9998994, 0.9999874 });
	Register<float>::getinstance()->add_dist({ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 });
	Register<float>::getinstance()->add_dist({ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 });

	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
	//Register<float>::getinstance()->add_dist({ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });

	/*Register<float>::getinstance()->log(rmult, 0.4566722);
	Register<float>::getinstance()->log(radd, 0.45816374);
	Register<float>::getinstance()->log(rsub, 0.40511775);
	Register<float>::getinstance()->log(rdiv, 0.48526615);
	Register<float>::getinstance()->log(rclamp, 0.3093697);
	Register<float>::getinstance()->log(vdot, 0.35490173);
	Register<float>::getinstance()->log(rcos, 0.38528606);
	Register<float>::getinstance()->log(rexp, 0.36182967);
	Register<float>::getinstance()->log(rmax, 0.33782107);
	Register<float>::getinstance()->log(rmin, 0.3439123);
	Register<float>::getinstance()->log(rpow, 0.40450582);
	Register<float>::getinstance()->log(rsin, 0.44388217);
	Register<float>::getinstance()->log(rsqrt, 0.35003844);
	Register<float>::getinstance()->log(rtan, 0.42319456);*/
	
	

	Register<float>::getinstance()->log(1.0f, true, -1);
	Register<float>::getinstance()->log(2.0f, true, -1);
	Register<float>::getinstance()->log(3.14159265358979323846264338f, true, -1);
	Register<float>::getinstance()->log(4.0f, true, -1);

	// random noise
	Register<float>::getinstance()->log(22.0f, true, -1);
	Register<float>::getinstance()->log(13.0f, true, -1);
	Register<float>::getinstance()->log(5.0f, true, -1);
	Register<float>::getinstance()->log(10.0f, true, -1);

	std::shared_ptr<std::vector<std::string>> att_format = std::make_shared<std::vector<std::string>>();
	att_format->push_back("INFIX+");
	att_format->push_back("INFIX-");
	att_format->push_back("INFIX*");
	att_format->push_back("INFIX/");
	att_format->push_back("std::sqrt");
	att_format->push_back("std::sin");
	att_format->push_back("std::cos");
	att_format->push_back("std::tan");
	att_format->push_back("std::pow");
	att_format->push_back("std::exp");
	att_format->push_back("std::fmin");
	att_format->push_back("std::fmax");
	att_format->push_back("glm::dot");
	att_format->push_back("clamp");

	std::string input_path = "C:\\Users\\James-MSI\\Desktop\\_vecs_dataset.csv";

	/*std::shared_ptr<IRParser<float>> irp = std::make_shared<IRParser<float>>(att_format);
	irp->load_string("INFIX/(std::sin(float(13.000000)),INFIX-(float(22.000000),std::tan(INFIX/(std::cos(INFIX/(std::sin(std::tan(INFIX/(INFIX*(float(13.000000),std::sin(std::tan(INFIX+(float(13.000000),float(13.000000))))),INFIX/(INFIX-(INFIX+(INFIX*(float(2.000000),float(22.000000)),clamp(std::sin(glm::dot(vec3(P[2]),vec3(P[0]))))),std::fmin(std::sqrt(float(3.141593)),float(1.000000))),float(22.000000))))),INFIX-(float(22.000000),std::tan(float(13.000000))))),INFIX/(std::cos(INFIX/(std::sqrt(float(10.000000)),INFIX/(INFIX-(float(3.141593),float(2.000000)),float(1.000000)))),std::pow(float(22.000000),glm::dot(vec3(P[0]),vec3(P[2]))))))))");
	std::shared_ptr<ProgramHandle> ast = std::static_pointer_cast<ProgramHandle>(irp->parse());

	std::shared_ptr<CppTranslator> cpp = std::make_shared<CppTranslator>("float");
	ast->accept(cpp);
	std::cout << cpp->get_and_delete() << std::endl;
	return 0;*/

	FileIO fio;
	auto data = fio.load(input_path);
	auto format = fio.format;
	for (int i = 0; i < format->size(); i++) {
		if (format->at(i)) Register<float>::getinstance()->log(NAN, false, i);
		else Register<glm::vec3>::getinstance()->log(glm::vec3(NAN), false, i);
	}


	//std::shared_ptr<FitnessFunction> ff = std::make_shared<FitnessFunction>(data);
	//ff->test_sample(ast, 10000);
	//return 0;

	//BrdfReader brdfr;
	//double* brdf;
	//if (!brdfr.read_brdf("C:\\Users\\James-MSI\\Desktop\\yellow-plastic.binary", brdf)) {
	//	fprintf(stderr, "Error reading \n");
	//	exit(1);
	//}
	//StochasticMath sm;
	//for (int i = 0; i < 100; i++) {
	//	int sel = sm.randint(0, data->size() - 1);
	//	std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> datum = data->at(sel);
	//	glm::vec3 L = *(std::static_pointer_cast<glm::vec3>(datum->at(0)->value()));
	//	glm::vec3 V = *(std::static_pointer_cast<glm::vec3>(datum->at(1)->value()));

	//	float theta_in = std::acos(L.z);
	//	float phi_in = std::atan(L.y / (float)L.x);

	//	float theta_out = std::acos(V.z);
	//	float phi_out = std::atan(V.y / (float)V.x);

	//	double r, g, b;

	//	brdfr.lookup_brdf_val(brdf, theta_in, phi_in, theta_out, phi_out, r, g, b);
	//	std::cout << "L: " << L.x << "," << L.y << "," << L.z << ";  V: " << V.x << "," << V.y << "," << V.z << ";  out: " << r << "," << g << "," << b << std::endl;
	//}
	//return 0;

	/*FitnessFunction ff(data);
	for (int i = 0; i < 100; i++) {
		std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> datum = data->at(i);
		glm::vec3 V = *(std::static_pointer_cast<glm::vec3>(datum->at(1)->value()));
		glm::vec3 R = *(std::static_pointer_cast<glm::vec3>(datum->at(4)->value()));
		float gtres = *(std::static_pointer_cast<float>(ff.groundtruth(data->at(i))->value()));
		float evres = evolved(V, R);
		std::cout << gtres << "   " << evres << std::endl;
	}
	return 0;*/

	//std::shared_ptr<IRPopulationGenerator> irpg = std::make_shared<IRPopulationGenerator>();
	//
	//int samples_per_program = 20;
	//int program_count = 1000000;
	//int maxdepth = 4;
	//
	//std::string loc = "G:\\";
	//std::string base = "vec3_moreinputs";
	//std::string ext = "_0";
	//
	//irpg->generate_dataset<float>(maxdepth, program_count, loc, base + ext, 0.01, false, false);

	//for (int i = 0; i < maxdepth + 1; i++) {
	//	const std::string& ss = (loc + base + ext + std::to_string(i) + ".txt");
	//	const char * s = ss.c_str();
	//	std::remove(s);
	//}

	//std::shared_ptr<std::vector<std::string>> ins = std::make_shared<std::vector<std::string>>();
	//for (int i = 0; i < format->size(); i++) {
	//	ins->push_back(loc + base + "_is" + ext + "_" + std::to_string(i) + ".csv");
	//}

	//extract_and_sample<float>(input_path, att_format, loc+base+ext+".txt", loc+base+"_os"+ext+".csv", ins, loc+base+"_at"+ext+".csv", program_count, samples_per_program);
	//std::cout << "Extracting Validation sets:" << std::endl;
	//for (int i = 1; i < maxdepth+1; i++) {
	//	auto ins_val = std::make_shared<std::vector<std::string>>();
	//	for (int j = 0; j < format->size(); j++) {
	//		ins_val->push_back(loc + base + "_is" + ext + std::to_string(i) + std::to_string(j) + "_val.csv");
	//	}
	//	extract_and_sample<float>(input_path, att_format, loc+base+ext+std::to_string(i)+"_val.txt", loc+base+"_os"+ext+std::to_string(i)+"_val.csv", ins_val, loc+base+"_at"+ext+std::to_string(i)+"_val.csv", 100000, samples_per_program);
	//}

	int iterations = 10;
	int time = 10 * 60;

	std::shared_ptr<FitnessFunction> ff = std::make_shared<FitnessFunction>(data);
	std::shared_ptr<IslandModel> isl = std::make_shared<IslandModel>(ff);
	//std::string folder = "C:\\Users\\James-MSI\\Desktop\\Results\\YellowPlastic\\Standard\\Green\\";
	std::string folder = "C:\\Users\\James-MSI\\Desktop\\Results\\cookt_1\\temp\\";

	std::shared_ptr<std::vector<std::shared_ptr<GPLauncher<float>>>> threads = std::make_shared<std::vector<std::shared_ptr<GPLauncher<float>>>>();	
	std::shared_ptr<std::vector<int>> depths = std::make_shared<std::vector<int>>();
	for (int i = 0; i < 7; i++) threads->push_back(std::make_shared<GPLauncher<float>>(data, 1000, i));
	depths->push_back(3); depths->push_back(4);
	depths->push_back(5); depths->push_back(5);
	depths->push_back(6); depths->push_back(7);
	depths->push_back(8);
	float migration_rate = 0.005;

	for (int i = 0; i < iterations; i++) {
		isl->begin(threads, depths, data, migration_rate, folder + "gpp_" + std::to_string(i) + ".csv", folder + "gpmse_" + std::to_string(i) + ".csv", folder + "gpsze_" + std::to_string(i) + ".csv", folder + "gpi_" + std::to_string(i) + ".csv", folder + "gp_attenuation_" + std::to_string(i) + ".csv", time);
	}
	
	//std::shared_ptr<CommonPool> pool = isl->pool;
	//std::shared_ptr<CppTranslator> cpp = std::make_shared<CppTranslator>("float");
	//std::pair<std::shared_ptr<ProgramHandle>, double> pf;
	//int min = -1;
	//std::shared_ptr<ProgramHandle> best;
	//while ((pf = pool->pop()).first != nullptr) {
	//	if (min == -1 || min > pf.second) {
	//		min = pf.second;
	//		best = pf.first;
	//	}
	//}
	//best->accept(cpp);
	//std::cout << cpp->get_and_delete() << std::endl;
	//std::cout << "FIT: " << min << std::endl << std::endl;

	return 0;
}