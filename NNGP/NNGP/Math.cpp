#include "Math.hpp"

double Math::fact(unsigned int n){
	double ret = 1;
	for (unsigned int i = 1; i <= n; ++i)
		ret *= i;
	return ret;
}