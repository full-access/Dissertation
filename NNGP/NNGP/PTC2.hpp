#pragma once
#ifndef PTC2_HPP
#define PTC2_HPP

#include <memory>
#include "AbstractNode.hpp"
#include "Register.hpp"
#include "StochasticMath.hpp"

class PTC2 {
public:
	/* 
	D = maximum depth bound
	S = maximum size bound
	N = nonterminal set
	T = terminal set
	F = N.T
	qt = probability of choosing t in T
	qn = probability of choosing n in N
	W = {w1, w2, ... , wS} tree size probabilities
	*/
	std::shared_ptr<AbstractNode> generate(int D, int S) {

	}
};

#endif