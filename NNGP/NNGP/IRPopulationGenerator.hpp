//#pragma once
//#ifndef IRPOPULATIONGENERATIOR_HPP
//#define IRPOPULATIONGENERATIOR_HPP
//
//#include "IntermediateRepresentationTranslator.hpp"
//#include "GPIRFilter.hpp"
//#include "GPIROptimiser.hpp"
//#include "IRSet.hpp"
//#include "RHHSynthesizer.h"
//#include "FileIO.hpp"
//#include <stdio.h>
//#include "AbstractType.hpp"
//#include "Type.hpp"
//
//class IRPopulationGenerator {
//public:
//	void generate_dataset(int maxdepth, int popcount, std::string filepath, bool optimisations) {
//		std::shared_ptr<RHHSynthesizer> rhh = std::make_shared<RHHSynthesizer>();
//		std::shared_ptr<FileIO> filehandler = std::make_shared<FileIO>(filepath);
//		std::shared_ptr<IRSet> irset = std::make_shared<IRSet>();
//		GPIROptimiser gpiro(irset);
//		GPIRFilter gpirf;
//		std::shared_ptr<IntermediateRepresentationTranslator> irt = std::make_shared<IntermediateRepresentationTranslator>();
//
//		std::cout << "Generating dataset with popcount " + std::to_string(popcount) + " at maxdepth " + std::to_string(maxdepth) + "." << std::endl;
//		int continues = 0;
//
//		// ramped half-and-half generation loop
//		for (int i = popcount; i > 0; i--) {
//
//			if (continues >= 10000) {
//				std::cout << std::endl << "DATASET STAGNATED: Terminating dataset generation" << std::endl;
//				break;
//			}
//
//			if (i % ((int)(popcount/(float)100)+1) == 0) {
//				std::cout << std::to_string(popcount - i) + " / "+std::to_string(popcount)+" ("+std::to_string(((popcount - i)/(float)popcount)*100)+"%) units processed.\r" << std::flush;
//			}
//			int depth = std::ceil((float)(maxdepth*(i/(float)popcount)));
//			bool grow = (i % 2 == 0) ? true : false;
//			std::shared_ptr<AbstractNode> program = std::static_pointer_cast<AbstractNode>(rhh->force_accept(std::make_shared<Type<float>>(), depth, grow, true));
//			program->accept(irt);
//			std::shared_ptr<std::string> ir = std::make_shared<std::string>(irt->get_and_delete());
//			std::string temp = *ir;
//			if (optimisations) {
//				if (gpirf.blacklisted(ir)) {
//					i = i + 1;
//					continues += 1;
//					continue;
//				}
//				if (gpiro.generate_optimisation(ir, true)) {
//					if (*ir != temp) {
//						if (gpirf.blacklisted(ir)) {
//							i = i + 1;
//							continues += 1;
//							continue;
//						}
//					}
//				}
//				else {
//					i = i + 1;
//					continues += 1;
//					continue;
//				}
//			}
//			continues = 0;
//			filehandler->save_program(*ir);
//		}
//		std::cout << std::endl << "IR Population Generation Complete." << std::endl;
//	}
//};
//
//
//#endif 


#pragma once
#ifndef IRPOPULATIONGENERATIOR_HPP
#define IRPOPULATIONGENERATIOR_HPP

#include "IntermediateRepresentationTranslator.hpp"
#include "GPIRFilter.hpp"
#include "GPIROptimiser.hpp"
#include "IRSet.hpp"
#include "RHHSynthesizer.h"
#include "FileIO.hpp"
#include <stdio.h>
#include "AbstractType.hpp"
#include "Type.hpp"
#include <iostream>

class IRPopulationGenerator {
public:

	int island_id;

	IRPopulationGenerator(int island_id) {
		this->island_id = island_id;
	}

	int point(int current, int maxdepth, int count) {
		int sum = (maxdepth / (float)2)*(1 + maxdepth);
		return (int)(count * (current / (float)sum));
	}

	void goback(std::ifstream& f) {
		f.clear();
		f.seekg(0, std::ios::beg);
	}

	void get_inp(std::ifstream& f, std::string& str) {
		if (std::getline(f, str)) {
			while (str == "$$" || str == "") {
				if (!std::getline(f, str)) {
					goback(f);
					get_inp(f, str);
				}
			}
		}else {
			goback(f);
			get_inp(f, str);
		}
		if (str[str.length() - 1] != ')') {
			std::cout << "breakpoint";
		}
	}

	template <typename T>
	void generate_dataset(int maxdepth, int popcount, std::string destination, std::string filename, float val_split, bool optimisations, bool duplicate_removal) {

		std::shared_ptr<std::vector<std::shared_ptr<FileIO>>> filehandlers = std::make_shared<std::vector<std::shared_ptr<FileIO>>>();
		std::shared_ptr<std::vector<std::shared_ptr<FileIO>>> validationhandlers = std::make_shared<std::vector<std::shared_ptr<FileIO>>>();
		std::shared_ptr<std::vector<int>> counts = std::make_shared<std::vector<int>>();

		for (int i=1; i < maxdepth+1; i++) {
			filehandlers->push_back(std::make_shared<FileIO>(destination + filename + std::to_string(i) + ".txt"));
			validationhandlers->push_back(std::make_shared<FileIO>(destination + filename + std::to_string(i) + "_val.txt"));
			counts->push_back(0);
		}

		std::shared_ptr<RHHSynthesizer> rhh = std::make_shared<RHHSynthesizer>(island_id);
		std::shared_ptr<IRSet> irset = std::make_shared<IRSet>();
		GPIROptimiser gpiro(irset);
		GPIRFilter gpirf;
		std::shared_ptr<IntermediateRepresentationTranslator> irt = std::make_shared<IntermediateRepresentationTranslator>();

		std::cout << "Generating dataset with popcount " + std::to_string(popcount) + " at maxdepth " + std::to_string(maxdepth) + "." << std::endl;
		int continues = 0;
		int currdepth = 1;
		int progs = 0;

		int sel = point(currdepth, maxdepth, popcount);

		// ramped half-and-half generation loop
		for (int i = popcount; i > 0; i--) {

			if (continues >= 100000) {
				if (currdepth < maxdepth) {
					std::cout << std::endl << "Dataset stagnation detected at depth " << std::to_string(currdepth) << "; increasing to " << std::to_string(currdepth + 1) << "." << std::endl;
					currdepth++;
					sel += point(currdepth, maxdepth, popcount);
				}
				else {
					std::cout << "Total dataset stagnation detected; terminating generation." << std::endl;
					break;
				}
			}

			// generate programs with depths with ranked proportions
			// e.g. rhh with max depth 5 will generate programs with depth proportions 1:2:3:4:5
			// since there are more programs of depth 5 than 4, 3, 2, and 1.
			if ((popcount-i) == sel && currdepth < maxdepth) {
				currdepth++;
				std::cout << "Advancing average depth generation to " << currdepth << "..." << std::endl;
				progs = 0;
				sel += point(currdepth, maxdepth, popcount);
			}
			//int depth = (int)StochasticMath().ranksel(currdepth+1) + 1;
			int depth = currdepth;
			//if (depth > currdepth) depth = currdepth;

			// old code: will generate programs with uniform depths 1:1:1:1...
			//int depth = std::ceil((float)(maxdepth*(i/(float)popcount)));

			if (i % ((int)(popcount / (float)100) + 1) == 0) {
				std::cout << std::to_string(popcount - i) + " / " + std::to_string(popcount) + " (" + std::to_string(((popcount - i) / (float)popcount) * 100) + "%) units processed.\r" << std::flush;
			}

			bool grow = (i % 2 == 0) ? true : false;
			std::shared_ptr<AbstractNode> program = std::static_pointer_cast<AbstractNode>(rhh->force_accept(std::make_shared<Type<T>>(), depth, grow, true));
			int height = program->getdepth();
			if (height == 0) {
				i = i + 1;
				continue;
			}
			program->accept(irt);
			std::shared_ptr<std::string> ir = std::make_shared<std::string>(irt->get_and_delete());
			std::string temp = *ir;

			bool val = val_split > (StochasticMath().randint(0, 100000) / (float)100000);

			if (duplicate_removal || optimisations) {
				if (duplicate_removal && !val && irset->exists(temp)) {
					i = i + 1;
					continues += 1;
					continue;
				}
				if (optimisations) {
					if (gpiro.generate_optimisation(ir, true)) {
						if (*ir != temp) {
							if (gpirf.blacklisted(ir)) {
								i = i + 1;
								continues += 1;
								continue;
							}
						}
					}
					else {
						i = i + 1;
						continues += 1;
						continue;
					}
				}
			}
			continues = 0;
			if (val) {
				validationhandlers->at(height-1)->save_program(*ir);
				i = i - 1;
			}else {
				filehandlers->at(height-1)->save_program(*ir);
				progs++;
				counts->at(height - 1)++;
			}
		}
		std::cout << std::endl << "IR Population Generation complete, final counts are as follows:" << std::endl;
		for (int i = 0; i < counts->size(); i++) {
			std::cout << "Height " << i << ": " << counts->at(i) << std::endl;
		}

		std::string temp;
		std::shared_ptr<std::vector<std::ifstream>> streams = std::make_shared<std::vector<std::ifstream>>();
		for (int i = 0; i < maxdepth; i++) {
			streams->push_back(std::ifstream());
			streams->at(i).open(destination + filename + std::to_string(i+1) + ".txt");
			if (!streams->at(i)) {
				std::cerr << "Unable to open file datafile";
				exit(1);
			}
			std::getline(streams->at(i), temp);
			std::getline(streams->at(i), temp);
		}

		std::shared_ptr<FileIO> out = std::make_shared<FileIO>(destination+filename+".txt");
		currdepth = 1;
		sel = point(currdepth, maxdepth, popcount);
		int depth_sel = 0;
		std::string get;

		for (int i = 0; i < popcount; i++) {
			//if (i == sel && currdepth < maxdepth) {
			//	currdepth++;
			//	sel += point(currdepth, maxdepth, popcount);
			//}
			depth_sel = (int)StochasticMath().ranksel(maxdepth*2)+1;
			depth_sel = std::fmin(std::ceil(depth_sel / (float)2), maxdepth) - 1;
			get = "";
			get_inp(streams->at(depth_sel), get);
			out->save_program(get);
		}
	}
};


#endif 