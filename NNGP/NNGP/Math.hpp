#ifndef MATH_H
#define MATH_H

// Contains math methods to solve generic problems
static class Math {
public:
	// Iterative factorial function.
	static double fact(unsigned int n);
};

#endif