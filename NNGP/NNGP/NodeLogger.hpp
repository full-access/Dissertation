#pragma once
#ifndef NODELOGGER_HPP
#define NODELOGGER_HPP

#include "SelectionSet.hpp"
#include "AbstractType.hpp"
#include "AbstractTypeVisitor.hpp"
#include "AbstractVisitor.hpp"

class NodeLogger : public AbstractVisitor, public AbstractTypeVisitor, public std::enable_shared_from_this<NodeLogger> {
private:
	std::shared_ptr<SelectionSet> set;
	std::shared_ptr<Nonterminal> temp;
public:
	NodeLogger() {
		set = std::make_shared<SelectionSet>();
	}
	std::shared_ptr<SelectionSet> get_and_delete() {
		std::shared_ptr<SelectionSet> temp = set;
		set = std::make_shared<SelectionSet>();
		return temp;
	}

	virtual void visit(std::shared_ptr<Nonterminal> a) override {
		std::shared_ptr<AbstractType> type = a->behaviour->type;
		temp = a;
		type->accept(shared_from_this(), 1, false);
		for (int i = 0; i < a->parameters->size(); i++) {
			a->parameters->at(i)->accept(shared_from_this());
		}
	}

	// some janky-ass code I needed to get done really quick
	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<float>>, int depth, bool grow) override {
		set->floats->insert(temp);
		return std::static_pointer_cast<void>(std::make_shared<bool>(true));
	}
	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<glm::vec3>>, int depth, bool grow) override {
		set->vecs->insert(temp);
		return std::static_pointer_cast<void>(std::make_shared<bool>(true));
	}

	virtual void visit(std::shared_ptr<Terminal<float>> a) override {
		set->floats->insert(a);
	}
	virtual void visit(std::shared_ptr<Terminal<glm::vec3>> a) override {
		set->vecs->insert(a);
	}

	virtual void visit(std::shared_ptr<Terminal<std::shared_ptr<void>>>) {};
};

#endif