#ifndef GPPROGRAM_H
#define GPPROGRAM_H

#include "AbstractNodeWrapper.hpp"
#include "Terminal.hpp"
#include <vector>


// Pretty sure I'll delete this class later
class GPProgram {
public:
	std::shared_ptr<AbstractNodeWrapper> program;
	GPProgram(std::shared_ptr<AbstractNodeWrapper> program) {
		this->program = program;
	}
};

#endif