#ifndef E_UNINITIALIZED_TERMINAL_H
#define E_UNINITIALIZED_TERMINAL_H

#include <iostream>
#include <exception>
#include <stdexcept>
#include <sstream>

class E_Uninitialized_Terminal : public std::runtime_error {
public:
	E_Uninitialized_Terminal() : std::runtime_error("Terminal is uninitialized") {}
};

#endif