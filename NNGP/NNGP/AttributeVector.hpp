#pragma once
#ifndef ATTRIBUTEVECTOR_HPP
#define ATTRIBUTEVECTOR_HPP

#include <memory>
#include <vector>
#include <map>

class AttributeVector {
public:
	std::shared_ptr<std::map<std::string, int>> attrib_freqs;
	AttributeVector(std::shared_ptr<std::vector<std::string>> format) {
		attrib_freqs = std::make_shared<std::map<std::string, int>>();
		for (int i = 0; i < format->size(); i++) {
			(*attrib_freqs)[format->at(i)] = 0;
		}
	}
	void incr_attrib(std::string displayname) {
		(*attrib_freqs)[displayname] += 1;
	}
};

#endif