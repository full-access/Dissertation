#ifndef CPPTRANSLATOR_H
#define CPPTRANSLATOR_H

#include "AbstractVisitor.hpp"

class CppTranslator : public AbstractVisitor, public std::enable_shared_from_this<CppTranslator>{
private:
	std::string program;
	std::string includes;
	std::string ret_type;
public:
	CppTranslator(std::string ret_type) {
		this->ret_type = ret_type;
		includes = "#include <iostream>\n#include <math.h>";
		program = includes+"\nint main(int argc, char *argv[]){\n\t"+ ret_type +" ret = ";
	}
	std::string get_and_delete() {
		program += ";\n\tstd::cout << ret << std::endl; \n\treturn 0;\n}";
		std::string temp = program;
		includes = "#include <iostream>\n#include <math.h>";
		program = includes + "\nint main(int argc, char *argv[]){\n\t"+ ret_type +" ret = ";
		return temp;
	}
	virtual void visit(std::shared_ptr<Nonterminal> a) override {
		if (a->behaviour->displayname.find("INFIX") != std::string::npos) {
			program += "(";
			a->parameters->at(0)->accept(shared_from_this());
			program += " ";
			program += a->behaviour->displayname[5];	// hence why it only supports 1 character
			program += " ";
			a->parameters->at(1)->accept(shared_from_this());
			program += ")";
		}
		else {
			program += a->behaviour->displayname + "(";
			for (int i = 0; i < a->parameters->size(); i++) {
				a->parameters->at(i)->accept(shared_from_this());
				if (i < a->parameters->size() - 1) program += ", ";
			}
			program += ")";
		}
	}
	virtual void visit(std::shared_ptr<Terminal<float>> a) override {
		if(a->varid > -1)
			program += "argv[" + std::to_string(a->varid + 1) + "]";
		else 
			program += std::to_string(a->val);
	}
	virtual void visit(std::shared_ptr<Terminal<glm::vec3>> a) override {
		if (a->varid > -1)
			program += "argv[" + std::to_string(a->varid + 1) + "]";
		else
			program += "glm::vec3(" + std::to_string(a->val.x) + ", " + std::to_string(a->val.y) + ", " + std::to_string(a->val.z) + ")";
	}
	virtual void visit(std::shared_ptr<Terminal<std::shared_ptr<void>>> a) override {
	}
};

#endif