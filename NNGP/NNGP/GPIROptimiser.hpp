#ifndef GPIROPT_HPP
#define GPIROPT_HPP

#include "OptDirective.hpp"
#include "IRSet.hpp"
#include <memory>

class GPIROptimiser{
private:
	std::vector<OptDirective> directives;
	std::shared_ptr<IRSet> irset;
public:

	// The GPF only recognises floats with 6 DECIMAL PLACES (TACKY BUT FAST)
	GPIROptimiser(std::shared_ptr<IRSet> irset) {
		this->irset = irset;

		std::vector<std::string> regexes;
		std::vector<std::string> replacements;

		std::string slash = "\\/";
		std::string text = "[a-zA-Z0-9\\[\\]\\.*:+-"+ slash +"]*";
		std::string function = "(?<function>(" + text + "\\(((?&function)(\\,(?&function))*)\\))|" + text + ")";

		// sqrt(x) * sqrt(x) = x
		regexes.push_back("INFIX\\*\\(std::sqrt\\(float\\((" + text + ")\\)\\)\\,std::sqrt\\(float\\(\\1\\)\\)\\)");
		replacements.push_back("float(\\1)");
		// sqrt(x) * sqrt(y) = sqrt(x*y)
		regexes.push_back("INFIX\\*\\(std::sqrt\\(float\\((" + text + ")\\)\\)\\,std::sqrt\\(float\\(((?!\\1)" + text + ")\\)\\)\\)");
		replacements.push_back("std::sqrt\\(INFIX\\*\\(float\\(\\1\\)\\,float\\(\\2\\)\\)\\)");
		// sqrt(0) = 0
		regexes.push_back("std::sqrt\\(float\\([0.]*\\)\\)");
		replacements.push_back("float(0.000000)");
		// sqrt(0) = 0
		regexes.push_back("std::sqrt\\(float\\(1.[0]+\\)\\)");
		replacements.push_back("float(1.000000)");

		// x * 0 = 0 (this is a bit complicated, but remember that <fun> = <text> | <text>(<fun>(,<fun>)*)) so mul(<fun>,0) -> 0
		regexes.push_back("INFIX\\*\\("+ function +"\\,float\\([0.]+\\)\\)");
		replacements.push_back("float(0.000000)");
		// 0 * x = 0
		regexes.push_back("INFIX\\*\\(float\\([0.]*\\)\\,"+ function +"\\)");
		replacements.push_back("float(0.000000)");
		// x * 1 = x
		// 1 * x = x

		// x - x = 0
		regexes.push_back("INFIX\\-\\("+function+"\\,(\\1)\\)");
		replacements.push_back("float(0.000000)");
		// x - 0 = x
		regexes.push_back("INFIX\\-\\(" + function + "\\,float\\([0.]+\\)\\)");
		replacements.push_back("\\1");

		// x + 0 = x
		regexes.push_back("INFIX\\+\\(" + function + "\\,float\\([0.]+\\)\\)");
		replacements.push_back("\\1");
		// 0 + x = x
		regexes.push_back("INFIX\\+\\(float\\([0.]+\\)\\," + function + "\\)");
		replacements.push_back("\\1");

		// x / x = 1
		regexes.push_back("INFIX"+ slash +"\\("+function+"\\,(\\1)\\)");
		replacements.push_back("float(1.000000)");
		// x / 1 = x
		regexes.push_back("INFIX"+ slash +"\\("+function+"\\,float\\(1.[0]+\\)\\)");
		replacements.push_back("\\1");


		// c + c = c
		// c * c = c
		// c / c = c
		// sqrt(c) = c
		// sin(c) = c
		// cos(c) = c


		// sin2(x) + cos2(x) = 1
		regexes.push_back("INFIX\\+\\(INFIX\\*\\(std::sin\\((" + function + ")\\)\\,std::sin\\(\\1\\)\\)\\,INFIX\\*\\(std::cos\\(\\1\\)\\,std::cos\\(\\1\\)\\)\\)");
		replacements.push_back("float(1.000000)");
		// cos2(x) + sin2(x) = 1
		regexes.push_back("INFIX\\+\\(INFIX\\*\\(std::cos\\((" + function + ")\\)\\,std::cos\\(\\1\\)\\)\\,INFIX\\*\\(std::sin\\(\\1\\)\\,std::sin\\(\\1\\)\\)\\)");
		replacements.push_back("float(1.000000)");

		for (int i = 0; i < regexes.size(); i++) {
			directives.push_back(OptDirective(regexes.at(i), replacements.at(i)));
		}
	}

	bool generate_optimisation(std::shared_ptr<std::string> raw, bool exit_on_duplicate){
		if (irset->exists(*raw) && exit_on_duplicate) {
			return 0;
		}
		bool repeat = false;
		std::string temp = "";
		do {
			repeat = false;
			for (int i = 0; i < directives.size(); i++) {
				temp = directives.at(i).match_and_replace(*raw);
				if (temp != *raw) {
					if (irset->exists(temp) && exit_on_duplicate) {
						return 0;
					}
					*raw = temp;
					repeat = true;
				}
			}
		} while (repeat);
		return 1;
	}
};

#endif