#pragma once
#ifndef REGISTER_HPP
#define REGISTER_HPP

#include <memory>
#include <vector>
#include <unordered_map>
#include "GPBehaviourWrapper.hpp"
#include "GPTerminalWrapper.hpp"
#include <type_traits>
#include <mutex>

std::ostream &operator<< (std::ostream &out, const glm::vec3 &vec) {
	out << "(" << vec.x << "," << vec.y << "," << vec.z << ")";
	return out;
}
std::string to_string(glm::vec3 &vec) {
	std::string out = "("+std::to_string(vec.x)+","+std::to_string(vec.y)+","+std::to_string(vec.z)+")";
	return out;
}
std::string to_string(float &fl) {
	std::string out = std::to_string(fl);
	return out;
}

template <class T> class GPBehaviourWrapper;

// templated singleton -- many of these may exist in the system as global variables, all with different typeclasses
template <class T>
class Register {
private:
	inline static std::shared_ptr<Register<T>> _instance = 0;
public:
	// stores the indices of behaviours such that you can look up behaviour nodes from their displaynames
	inline static std::shared_ptr<std::unordered_map<std::string, int>> behaviour_index_lookup = std::make_shared<std::unordered_map<std::string, int>>();
	inline static std::shared_ptr<std::unordered_map<std::string, int>> constants_index_lookup = std::make_shared<std::unordered_map<std::string, int>>();

	inline static std::shared_ptr<std::vector<std::shared_ptr<T>>> constants = std::make_shared<std::vector<std::shared_ptr<T>>>();
	inline static std::shared_ptr<std::vector<std::pair<std::shared_ptr<T>, int>>> arguments = std::make_shared<std::vector<std::pair<std::shared_ptr<T>, int>>>();

	inline static std::shared_ptr<std::vector<std::shared_ptr<GPBehaviourWrapper<T>>>> behaviours = std::make_shared<std::vector<std::shared_ptr<GPBehaviourWrapper<T>>>>();
	inline static std::shared_ptr<std::vector<std::shared_ptr<std::vector<double>>>> probdist = std::make_shared<std::vector<std::shared_ptr<std::vector<double>>>>();
	inline static std::shared_ptr<std::vector<double>> totalprob = std::make_shared<std::vector<double>>();

	inline static int type = (int)0;	// temporary fix
	inline static std::shared_ptr<std::mutex> mut = std::make_shared<std::mutex>();

	// singleton creation
	static std::shared_ptr<Register<T>> getinstance() {
		mut->lock();
		if (_instance == 0) {
			if (std::is_same<T, float>::value) type = 0;	// TEMPORARY
			if (std::is_same<T, glm::vec3>::value) type = 1;
			_instance = std::make_shared<Register<T>>();
		}
		mut->unlock();
		return _instance;
	}

	// added these manual functions instead of autoregistration.
	// autoregistration must be perfromed in the constructor of
	// the object being registered. since it is not technically
	// a shared pointer within the constructor, the "this" keyword
	// doesnt allow for such registration
	static void log(T terminal, bool isconst, int varid) {
		mut->lock();
		if (isconst) {
			constants->push_back(std::make_shared<T>(terminal));
			(*constants_index_lookup)[to_string(terminal)] = constants->size() - 1;
		}else {
			arguments->push_back(std::make_pair(std::make_shared<T>(terminal), varid));
		}
		mut->unlock();
	}

	static void log(std::shared_ptr<GPBehaviourWrapper<T>> behaviour) {
		mut->lock();
		behaviours->push_back(behaviour);
		(*behaviour_index_lookup)[behaviour->displayname] = behaviours->size() - 1;
		mut->unlock();
	}

	static void add_dist(std::initializer_list<double> probs) {
		std::shared_ptr<std::vector<double>> ds = std::make_shared<std::vector<double>>();
		double tot = 0;
		for (double d : probs) {
			tot += d;
			ds->push_back(d);
		}
		totalprob->push_back(tot);
		probdist->push_back(ds);			
	}
};

#endif