#pragma once
#ifndef IRLEXER_HPP
#define IRLEXER_HPP

#include <memory>
#include <iostream>

class IRLexer{
private:
	std::string in;
	int index;
public:
	IRLexer() {}
	void load_string(std::string in) {
		this->in = in;
		index = 0;
	}
	std::string next() {
		if (index == in.length() - 1) return " "; // use a space to denote end-of-string
		bool cont = true;
		std::string ret = "";
		while(cont){
			if (in[index] == '(' || in[index] == ')' || in[index] == ',' || in[index] == '\0')	// microsyntax
				cont = false;
			else
				ret += in[index];
			index++;
		}
		return ret;
	}
};

#endif
