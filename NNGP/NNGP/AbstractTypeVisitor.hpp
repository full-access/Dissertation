#pragma once
#ifndef ABSTRACTTYPEVISITOR_HPP
#define ABSTRACTTYPEVISITOR_HPP

template <class T>
class Type;
#include <memory>
#include <glm\glm.hpp>

class AbstractTypeVisitor {
public:
	// these can probably be templated as Type<T> using static polymorphism
	// Base<T> -> abstract class (not templated) -> a number of derived classes like RHHSynthesizer
	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<float>>, int depth, bool grow) = 0;
	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<glm::vec3>>, int depth, bool grow) = 0;
};

#endif