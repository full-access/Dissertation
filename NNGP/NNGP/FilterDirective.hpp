#pragma once
#ifndef FILTDIR_HPP
#define FILTDIR_HPP

#include <boost/regex.hpp>

class FilterDirective {
private:
	boost::regex regex;
public:
	FilterDirective(std::string regex) {
		this->regex = boost::regex{ regex };
	}
	bool match(std::string input) {
		return boost::regex_search(input, this->regex);
	}
};

#endif