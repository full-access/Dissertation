#include <string>
#include <memory>
#include <vector>
#include <algorithm>
#include <random>
#include "StochasticMath.hpp"
#include "DyckWordMapper.hpp"

/*
Eg.:
	Assuming arities {1,2,3,4}...
	1. Configuration = (2 1 0 0), n = 5
	2. This is 2 ones, 1 twos, 0 threes and fours...
	3. Unravel it into 1,1,2
	4. Pack it with 0's to fit the node count of 5: 1,1,2,0,0
	5. Randomly shuffle: 1,0,2,1,0
	6. Transcribe this into a dyck word, while adding x's in between: xyxxyyxyx
*/
std::string DyckWordMapper::map(const std::unique_ptr<std::vector<int>>& arities, const std::shared_ptr<std::vector<int>> configuration, int node_count) {
	std::unique_ptr<std::vector<int>> tempvec(new std::vector<int>);
	unravel(arities, configuration, tempvec, node_count);
	StochasticMath().shuffle(tempvec);
	return to_dyck(tempvec);
}

// Eg. 1 -> "y", 2 -> "yy", 3 -> "yyy" ...
std::string DyckWordMapper::num_to_y(int n) {
	std::string ret = "";
	for (int i = 0; i < n; i++) {
		ret.append("y");
	}
	return ret;
}

// Eg. 1,1,2 with node count of 5 => 1,1,2,0,0 (a function that packs in zeros)
void DyckWordMapper::pack(std::unique_ptr<std::vector<int>>& in, int node_count, int nodes_added){
	for (int i = 0; i < node_count - nodes_added + 1; i++) {
		in->push_back(0);
	}
}

// Eg. 3,2,0,1 -> 1,1,1,2,2,4
void DyckWordMapper::unravel(const std::unique_ptr<std::vector<int>>& arities, const std::shared_ptr<std::vector<int>>& configuration, std::unique_ptr<std::vector<int>>& out, int node_count){
	int nodes_accounted_for = 0;
	for (int i = 0; i < configuration->size(); i++) {
		int elem = configuration->at(i);
		nodes_accounted_for += elem * (arities->at(i));
		for (int j = 0; j < elem; j++) {
			out->push_back(arities->at(i));
		}
	}
	pack(out, node_count, nodes_accounted_for);
}

// Eg. 10210 -> xyxxyyxyx
std::string DyckWordMapper::to_dyck(const std::unique_ptr<std::vector<int>>& in){
	std::string out = "";
	for (int i = 0; i < in->size(); i++) {
		out.append("x");
		out.append(num_to_y(in->at(i)));
	}
	return out;
}
