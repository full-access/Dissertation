#pragma once
#ifndef INPUTGENERATOR_HPP
#define INPUTGENERATOR_HPP

#include <iostream>
#include <fstream>

class InputGenerator {
public:
	void generate_floats(int num_samples, float minfloat, float maxfloat, std::string filepath) {
		std::ofstream storage;
		storage.open(filepath);
		float interval = (maxfloat - minfloat) / (float)(num_samples);
		float next_sample = 0;
		for (int i = 0; i < num_samples; i++) {
			next_sample = i * interval;
			storage << next_sample << "\n";
		}
		storage.close();
	}
};

#endif