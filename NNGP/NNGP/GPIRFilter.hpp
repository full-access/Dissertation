#ifndef GPIRFILT_HPP
#define GPIRFILT_HPP

#include "FilterDirective.hpp"
#include <memory>

class GPIRFilter {
private:
	std::vector<FilterDirective> directives;
public:
	GPIRFilter() {

		std::vector<std::string> regexes;

		std::string slash = "\\/";
		std::string text = "[a-zA-Z0-9\\[\\]\\.*:+-" + slash + "]*";
		std::string function = "(?<function>(" + text + "\\(((?&function)(\\,(?&function))*)\\))|" + text + ")";

		// x / 0
		regexes.push_back("INFIX\\*\\(std::sqrt\\(float\\((" + text + ")\\)\\)\\,std::sqrt\\(float\\(\\1\\)\\)\\)");
		// 

		for (int i = 0; i < regexes.size(); i++) {
			directives.push_back(FilterDirective(regexes.at(i)));
		}
	}

	bool blacklisted(std::shared_ptr<std::string> raw) {
		for (int i = 0; i < directives.size(); i++) {
			if (directives.at(i).match(*raw)) {
				return true;
			}
		}
		return false;
	}
};

#endif