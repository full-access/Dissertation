#ifndef TREEMATH_H
#define TREEMATH_H

// Contains math methods to solve some tree-based problems.
static class TreeMath {
public:
//	// Given a list of node constraints, will gneerate a separate list of corresponding tree node counts. This can then be used to stochastically select a configuration.
//	static std::unique_ptr<std::vector<double>> generate_treecount_list(std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>>& ss, unsigned int n);
//	// Stochastically select a function configuration such that it is consistent with a uniform selection of possible trees.
//	static std::shared_ptr<std::vector<int>> select_node_constraint(std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>>& ss, std::unique_ptr<std::vector<double>>& sizes);
//
//private:
//	// As per definition in [Iba1996].
//	static unsigned int get_residue(std::shared_ptr<std::vector<int>> s, unsigned int n);
//	// As per definition in [Iba1996].
//	static double distinct_tree_count(std::shared_ptr<std::vector<int>> s, double fact_n, unsigned int n);
//	// Calculates the catalan number at index n.
//	static double catalan(unsigned int n);
//	// Stochastically select the number of nodes in your tree such that it is consistent with a uniform selection of possible trees.
	static int select_node_count(unsigned int minnode, unsigned int maxnode);
};

#endif