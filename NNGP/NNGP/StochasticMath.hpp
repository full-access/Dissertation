#ifndef STOCHASTICMATH_H
#define STOCHASTICMATH_H

#include <tuple>

// Contains math methods to solve stochastic problems.
class StochasticMath {
public:

	// we want to sort in descending order, as better fitnesses are smaller in value while having a larger rank.
	// fitnesses:       1000    52    43    25    12    9    1 (descending)
	// ranks:           1       2     3     4     5     6    7
	// selection:      unlikely -----------------------> most likely
	struct compare_tuples{
		inline bool operator() (const std::pair<double, int> a, const std::pair<double, int> b){
			return (a.first)>(b.first);
		}
	};

	// Generate a random integer between min and max (inclusive).
	int randint(int min, int max);
	void sort(std::shared_ptr<std::vector<std::pair<double, int>>> list);
	int StochasticMath::ranksel(int count);
	// Select an index to an element chosen randomly directly proportional to the size of their contents.
	unsigned int selprop(std::shared_ptr<std::vector<double>>& list, double sumlist);
	unsigned int selprop(std::initializer_list<float> list);
	// Select an index to an element chosen randomly directly proportional to the size of their contents.
	unsigned int selprop(std::shared_ptr<std::vector<double>>& list);
	// Select an index to an element chosen randomly directly proportional to the size of their contents.
	unsigned int selprop(std::unique_ptr<std::vector<int>>& list, double sumlist);
	// Select an index to an element chosen randomly directly proportional to the size of their contents.
	unsigned int selprop(std::unique_ptr<std::vector<int>>& list);
	// Randomly shuffles the contents of an integer vector.
	void shuffle(std::unique_ptr<std::vector<int>>& input);

	template <typename T>
	std::tuple<int, int> select_node(int number_of_behaviours, int number_of_constants, int number_of_variables, int island_id) {
		std::unique_ptr<std::vector<int>> input = std::make_unique<std::vector<int>>();
		input->push_back(number_of_behaviours);
		input->push_back(number_of_constants);
		input->push_back(number_of_variables);
		int grp = selprop(input, number_of_behaviours + number_of_constants + number_of_variables);
		int res = 0;
		if (grp == 0 && input->at(grp) > 1) {
			res = StochasticMath::selprop(Register<T>::getinstance()->probdist->at(island_id), Register<T>::getinstance()->totalprob->at(island_id));
		}
		else {
			if (input->at(grp) > 1) {
				res = randint(0, input->at(grp) - 1);
			}
		}
		return std::make_tuple(grp, res);
	}
};

#endif