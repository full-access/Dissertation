#pragma once
#ifndef GPBEHAVIOURWRAPPER_HPP
#define GPBEHAVIOURWRAPPER_HPP

#include "Behaviour.hpp"
#include "AbstractType.hpp"
#include "Register.hpp"
#include "Type.hpp"

/*
This class is a decorator that wraps over the behaviour class to include type retention features.
The base AST implicitly supports type erasure, which is not suitable for GP purposes since the GP must know the types for tree generation.
*/
template <class T>
class GPBehaviourWrapper : public Behaviour {
public:
	std::vector<std::shared_ptr<AbstractType>> paramtypes;
	GPBehaviourWrapper(int type_id, std::initializer_list<std::shared_ptr<AbstractType>> paramtypes) : Behaviour(paramtypes.size()) {
		this->type_id = type_id;
		this->type = std::make_shared<Type<T>>();
		this->displayname = "unnamed_behaviour";
		for (std::shared_ptr<AbstractType> t : paramtypes)
			this->paramtypes.push_back(t);
	}
	GPBehaviourWrapper(int type_id, std::string displayname, std::initializer_list<std::shared_ptr<AbstractType>> paramtypes): GPBehaviourWrapper(type_id, paramtypes){
		this->displayname = displayname;
	}
};

#endif