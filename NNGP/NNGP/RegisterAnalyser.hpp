#pragma once
#ifndef REGISTERANALYSER_HPP
#define REGISTERANALYSER_HPP

#include <memory>
#include <vector>
#include "Register.hpp"

template <class T>
class RegisterAnalyser {
public:
	static void print_register() {
		std::cout << "----- Register Contents -----" << std::endl;
		if (Register<T>::getinstance()->constants->size() > 0) {
			std::cout << "Terminals:" << std::endl;
			for (int i = 0; i < Register<T>::getinstance()->constants->size(); i++) {
				std::cout << *Register<T>::getinstance()->constants->at(i) << std::endl;
			}
			std::cout << std::endl;
		}
		if (Register<T>::getinstance()->behaviours->size() > 0) {
			std::cout << "NonTerminals:" << std::endl;
			for (int i = 0; i < Register<T>::getinstance()->behaviours->size(); i++) {
				std::cout << Register<T>::getinstance()->behaviours->at(i)->displayname << std::endl;
			}
		}
		std::cout << "------- End Contents --------" << std::endl;
	}
};

#endif