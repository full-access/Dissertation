#pragma once
#ifndef ABSTRACTVISITOR_HPP
#define ABSTRACTVISITOR_HPP

class Nonterminal;
template <class T>
class Terminal;
#include <memory>
#include <glm\glm.hpp>

class AbstractVisitor {
public:
	virtual void visit(std::shared_ptr<Nonterminal>) = 0;
	virtual void visit(std::shared_ptr<Terminal<float>>) = 0;
	virtual void visit(std::shared_ptr<Terminal<glm::vec3>>) = 0;
	virtual void visit(std::shared_ptr<Terminal<std::shared_ptr<void>>>) = 0;
};

#endif