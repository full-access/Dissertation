#ifndef FILEIO_H
#define FILEIO_H

#include <iostream>
#include <fstream>
#include <string>
#include "AttributeVector.hpp"
#include <cmath>
#include "TextToArgumentParser.hpp"

class FileIO {
public:
	std::ofstream storage;
	std::shared_ptr<std::vector<bool>> format;

	FileIO() {}
	FileIO(std::string path) {
		storage.open(path);	
		if (!storage) {
			std::cerr << "Unable to open file datafile";
			exit(1);
		}
	}

	std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> load(std::string path) {
		std::ifstream file;
		file.open(path);
		if (!file) {
			std::cerr << "Unable to open file datafile";
			exit(1);
		}
		std::string content((std::istreambuf_iterator<char>(file)),(std::istreambuf_iterator<char>()));
		return TextToArgumentParser().parse(content, format);
	}

	void save_float(std::shared_ptr<AbstractDatum> d, int multiplier, float offset) {
		float f = *(std::static_pointer_cast<float>(d->value()));
		if (std::isnan(f)) {
			storage << "NaN";
		}else {
			if (std::isinf(f)) {
				storage << "Inf";
			}else {
				storage << std::to_string((f * multiplier )+offset);
			}
		}
	}

	void save_vec(std::shared_ptr<AbstractDatum> d, int multiplier, float offset) {
		glm::vec3 v = *(std::static_pointer_cast<glm::vec3>(d->value()));
		float fs[3];
		for (int i = 0; i < 3; i++) {
			if (std::isnan(v[i])) {
				fs[i] = NAN;
			}
			else {
				if (std::isinf(v[i])) {
					fs[i] = NAN;
				}
				else {
					fs[i] = (v[i] * multiplier)+offset;
				}
			}			
		}
		storage << std::to_string(fs[0]) << "," << std::to_string(fs[1]) << "," << std::to_string(fs[2]);
	}

	void save_single_sample(bool isfloat, std::shared_ptr<AbstractDatum> sample,int multiplier, float offset) {
		if (isfloat) {
			save_float(sample, multiplier, offset);
		}
		else {
			save_vec(sample, multiplier, offset);
		}
		storage << std::endl;
	}

	void save_sample(std::shared_ptr<std::vector<float>> samples, int io_count_per_sample) {
		for (int i = 0; i < samples->size(); i++) {
			if (i % io_count_per_sample == 0 && i != 0) {
				storage << std::endl;
			}
			else {
				if (i != 0) storage << ",";
			}
			if (std::isnan(samples->at(i))) {
				storage << "NaN";
			}
			else {
				if (std::isinf(samples->at(i))) {
					storage << "Inf";
				}
				else {
					storage << std::to_string(samples->at(i));
				}
			}
		}
		storage << std::endl;
	}

	void save_program(std::string program) {
		if (program[program.length() - 1] != ')') {
			std::cout << "breakpoint";
		}
		storage << "\n$$\n" << program;
		storage.flush();
	}
	void save_attrib_vector(std::shared_ptr<AttributeVector> in) {
		std::map<std::string, int>::iterator it = in->attrib_freqs->begin();
		int i = 0;
		while (it != in->attrib_freqs->end()){
			if (i != 0) storage << ",";
			//if (it->second > 0) {
			//	storage << '1';
			//}
			//else {
			//	storage << '0';
			//}
			storage << it->second;
			it++;
			i++;
		}
		storage << "\n";
	}
	~FileIO() {
		storage.flush();
		storage.close();
	}
};

#endif