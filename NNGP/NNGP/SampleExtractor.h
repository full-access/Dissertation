#pragma once
#ifndef SAMPLEEXTRACTOR_HPP
#define SAMPLEEXTRACTOR_HPP

#include "AbstractNode.hpp"
#include <memory>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>

/*
Extracts input-output samples from a known AST

		AST ---- magic ---> n input-output examples
*/
class SampleExtractor{
public:
	std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> inputs;
	std::shared_ptr<std::vector<bool>> in_format;

	SampleExtractor(std::string dataset_path) {
		FileIO fio;
		inputs = fio.load(dataset_path);
		in_format = fio.format;
	}

	template <typename T>
	int generate_samples(std::string dataset_path, std::shared_ptr<AbstractNode> program, std::shared_ptr<FileIO> out, std::shared_ptr<std::vector<std::shared_ptr<FileIO>>> in, int input_multiplier, float input_offset, int num_training_samples) {

		bool out_is_float = true;

		for (int i = 0; i < in_format->size(); i++) {
			std::shared_ptr<std::vector<bool>> b = std::make_shared<std::vector<bool>>();
			b->push_back(in_format->at(i));
			in->at(i)->format = b;
		}

		int processed_inputs = 0;
		int misses = 0;
		
		std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> params = std::make_shared<std::vector<std::shared_ptr<AbstractDatum>>>();
		std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> ins = std::make_shared<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>>();
		std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> outs = std::make_shared<std::vector<std::shared_ptr<AbstractDatum>>>();
		
		int i = -1;
		StochasticMath sm;

		while(processed_inputs < num_training_samples) {
			i++;

			if (misses >= (inputs->size() - num_training_samples)) {
				return -1;
			}
			params->clear();

			int sel = -1;

			sel = sm.randint(0, inputs->size() - 1);
			for (int j = 0; j < inputs->at(sel)->size(); j++) {
				params->push_back(inputs->at(sel)->at(j));
			}

			std::shared_ptr<AbstractDatum> ret;

			if (std::is_same<T, float>::value) {
				float res = *std::static_pointer_cast<float>(program->resolve(params));
				if (std::isnan(res) || std::isinf(res)) {
					res = 0;
				}else {
					if (res < -4096 || res > 4096) {
						misses += 1;
						continue;
					}
					res = ((res + 4096) * 100) + 1;	// the effective embedding range is 1 - 2000000 (0 reserved for inf and NaN)
				}
				ret = std::make_shared <Datum<float>>(res);
			}
			else {
				out_is_float = false;

				glm::vec3 res = *std::static_pointer_cast<glm::vec3>(program->resolve(params));
				bool cont = false;
				for (int k = 0; k < 3; k++) {
					if (std::isnan(res[k]) || std::isinf(res[k])) {
						res[k] = 0;
					}
					else {
						if (res[k] < -4096 || res[k] > 4096) {
							misses += 1;
							cont = true;
							break;
						}
						res = ((res + glm::vec3(4096)) * 100.0f) + glm::vec3(1);
					}					
				}
				if (cont) {
					cont = false;
					break;
				}
				ret = std::make_shared<Datum<glm::vec3>>(res);
			}
			outs->push_back(ret);

			ins->push_back(std::make_shared<std::vector<std::shared_ptr<AbstractDatum>>>());
			for (int k = 0; k < inputs->at(sel)->size(); k++) {
				ins->at(processed_inputs)->push_back(inputs->at(sel)->at(k));
			}
			processed_inputs++;
		}

		for (int p = 0; p < outs->size(); p++) {
			out->save_single_sample(out_is_float, outs->at(p), 1, 0);
			for (int q = 0; q < in_format->size(); q++) {
				in->at(q)->save_single_sample(in_format->at(q), ins->at(p)->at(q), input_multiplier, input_offset);
			}
		}
		ins->clear();

		return processed_inputs;
	}
};

#endif