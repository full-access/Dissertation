#ifndef DATUM_H
#define DATUM_H

#include "AbstractDatum.hpp"

template <class T>
class Datum : public AbstractDatum{
private:
	T val;
public:
	Datum(T val) { this->val = val; }
	virtual std::shared_ptr<void> value() override {
		return std::static_pointer_cast<void>(std::make_shared<T>(val));
	}
};

#endif