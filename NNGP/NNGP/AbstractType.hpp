#pragma once
#ifndef ABSTRACTYPE_HPP
#define ABSTRACTYPE_HPP

#include <boost\any.hpp>
#include "AbstractTypeVisitor.hpp"

class AbstractType {
public:
	// having the accept method return a void ptr and take extra parameters is NOT good programming practice
	// but I needed a hard and fast solution
	virtual std::shared_ptr<void> accept(std::shared_ptr<AbstractTypeVisitor> visitor, int depth, bool grow) = 0;
};

#endif