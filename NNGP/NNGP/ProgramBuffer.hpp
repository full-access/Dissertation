#pragma once
#ifndef PROGRAMBUFFER_HPP
#define PROGRAMBUFFER_HPP

#include <thread>
#include <mutex> 
#include <memory>
#include <queue>
#include "ProgramHandle.hpp"

class ProgramBuffer {
private:
	std::mutex lock;
	std::shared_ptr<std::queue<std::shared_ptr<ProgramHandle>>> buffer;
	int bufsize;
public:
	ProgramBuffer(int bufsize) {
		this->bufsize = bufsize;
		buffer = std::make_shared<std::queue<std::shared_ptr<ProgramHandle>>>();
	}
	int push(std::shared_ptr<ProgramHandle> p) {
		lock.lock();
		if (buffer->size() >= bufsize) {
			lock.unlock();
			return -1;
		}
		else {
			buffer->push(p);
			lock.unlock();
			return 0;
		}
	}
	std::shared_ptr<ProgramHandle> pop() {
		lock.lock();
		std::shared_ptr<ProgramHandle> ph;
		if (buffer->empty()) {
			ph = nullptr;
		}else {
			ph = buffer->front();
			buffer->pop();
		}
		lock.unlock();
		return ph;
	}
};

#endif