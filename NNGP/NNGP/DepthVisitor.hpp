#ifndef DEPTHVISITOR_HPP
#define DEPTHVISITOR_HPP

#include "AbstractVisitor.hpp"

class DepthVisitor : public AbstractVisitor{
private:
	int depth;
public:
	DepthVisitor() {
		depth = 0;
	}
	int get_and_delete() {
		int temp = depth;
		depth = 0;
		return temp;
	}
	virtual void visit(std::shared_ptr<Nonterminal> a) override {
		
	}
	virtual void visit(std::shared_ptr<Terminal<float>> a) override {
		if (a->varid > -1)
			program += "std::atof(argv[1])";
		else
			program += "float(" + std::to_string(a->val) + ")";
	}
	virtual void visit(std::shared_ptr<Terminal<glm::vec3>> a) override {
		program += "glm::vec3(" + std::to_string(a->val.x) + ", " + std::to_string(a->val.y) + ", " + std::to_string(a->val.z) + ")";
	}
	virtual void visit(std::shared_ptr<Terminal<std::shared_ptr<void>>> a) override {
	}
};

#endif