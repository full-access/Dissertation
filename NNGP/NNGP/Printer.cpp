#include "Printer.hpp"

void Printer::print_indent(){
	for (int i = 0; i < indent; i++) {
		std::cout << "|    ";
	}
}
void Printer::tabright(){
	this->indent ++;
}
void Printer::tableft(){
	this->indent --;
}

void Printer::visit(std::shared_ptr<Nonterminal> a) {
	print_indent();
	std::cout << a->behaviour->displayname << std::endl;
	for (int i = 0; i < a->parameters->size(); i++) {
		tabright();
		a->parameters->at(i)->accept(shared_from_this());
		tableft();
	}
}
void Printer::visit(std::shared_ptr<Terminal<float>> a) {
	print_indent();
//	std::cout << *(std::static_pointer_cast<float>(a->resolve())) << std::endl;
}
void Printer::visit(std::shared_ptr<Terminal<glm::vec3>> a) {
	//print_indent();
	//glm::vec3 v = *((glm::vec3*)a->resolve());
	//std::cout << "(" << v.x << "," << v.y << "," << v.z << ")" << std::endl;
}
void Printer::visit(std::shared_ptr<Terminal<std::shared_ptr<void>>> a) {
	//print_indent();
	//glm::vec3 v = *((glm::vec3*)a->resolve());
	//std::cout << "(" << v.x << "," << v.y << "," << v.z << ")" << std::endl;
}