#pragma once
#ifndef IRFILEEXTRACTOR_HPP
#define IRFILEEXTRACTOR_HPP

#include <memory>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>

class IRFileExtractor {
public:
	std::ifstream file;
	IRFileExtractor(std::string filepath) {
		file.open(filepath);
		if (!file) {
			std::cerr << "Unable to open file datafile.txt";
			exit(1);
		}
	}
	std::shared_ptr<std::vector<std::string>> extract(int amount, int& end) {
		end = 0;
		std::shared_ptr<std::vector<std::string>> irs = std::make_shared<std::vector<std::string>>();
		std::string temp;
		for (int i = 0; i < amount; i++) {
			if(std::getline(file, temp)) {
				if (temp == "$$" || temp == "") {
					i = i - 1;
					continue; // delimeter
				}
				irs->push_back(temp);
			}
			else {
				end = 1;
				return irs;
			}
		}
		return irs;
	}
	~IRFileExtractor() {
		file.close();
	}
};

#endif