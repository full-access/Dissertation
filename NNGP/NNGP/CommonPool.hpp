#pragma once
#ifndef COMMONPOOL_HPP
#define COMMONPOOL_HPP

#include <thread>
#include <mutex> 
#include <memory>
#include <deque>
#include <algorithm>
#include "ProgramHandle.hpp"
#include "FitnessFunction.hpp"

class CommonPool {
public:
private:
	std::mutex lock;
	std::shared_ptr<std::deque<std::pair<std::shared_ptr<ProgramHandle>, double>>> buffer;
	int bufsize;
public:
	std::shared_ptr<FitnessFunction> ff;
	CommonPool(int bufsize, std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> inputs) {
		this->ff = std::make_shared<FitnessFunction>(inputs);
		this->bufsize = bufsize;
		buffer = std::make_shared<std::deque<std::pair<std::shared_ptr<ProgramHandle>, double>>>();
	}
	void push(std::pair<std::shared_ptr<ProgramHandle>, double> p) {
		p.second = ff->fitness<float>(p.first, 100000);
		lock.lock();
		if (buffer->size() >= bufsize) {
			double max = -1;
			int ind = 0;
			for (int i = 0; i < buffer->size(); i++) {
				if (i == 0 || max < buffer->at(i).second) {
					max = buffer->at(i).second;
					ind = i;
				}
			}
			buffer->at(ind) = p;
			lock.unlock();
		}
		else {
			buffer->push_back(p);
			lock.unlock();
		}
	}
	std::pair<std::shared_ptr<ProgramHandle>, double> pop() {
		lock.lock();
		std::pair<std::shared_ptr<ProgramHandle>, double> ph;
		if (buffer->empty()) {
			ph = std::pair<std::shared_ptr<ProgramHandle>, double>(nullptr,-1);
		}
		else {
			ph = buffer->front();
			buffer->pop_front();
		}
		lock.unlock();
		return ph;
	}
	std::pair<std::shared_ptr<ProgramHandle>, double> peek(int i) {
		lock.lock();
		std::pair<std::shared_ptr<ProgramHandle>, double> ph;
		if (buffer->empty()) {
			ph = std::pair<std::shared_ptr<ProgramHandle>, double>(nullptr, -1);
		}
		else {
			ph = buffer->at(i);
		}
		lock.unlock();
		return ph;
	}
	int size() {
		return buffer->size();
	}
};

#endif