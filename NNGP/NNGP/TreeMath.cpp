#include <iostream>
#include <memory>
#include <vector>
#include <cmath>
#include "TreeMath.hpp"
#include "Math.hpp"
#include "StochasticMath.hpp"

//unsigned int TreeMath::get_residue(std::shared_ptr<std::vector<int>> s, unsigned int n) {
//	int ret = n;
//	for (int i = 0; i < s->size(); i++) {
//		ret = ret - s->at(i);
//	}
//	return ret;
//}
//
//double TreeMath::distinct_tree_count(std::shared_ptr<std::vector<int>> s, double fact_n, unsigned int n) {
//	double ret = fact_n / Math::fact(get_residue(s, n));
//	ret = ret / n;
//	for (int i = 0; i < s->size(); i++) {
//		ret = ret / Math::fact(s->at(i));
//	}
//	return ret;
//}
//
//std::unique_ptr<std::vector<double>> TreeMath::generate_treecount_list(std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>>& ss, unsigned int n) {
//	std::unique_ptr<std::vector<double>> tree_counts(new std::vector<double>);
//	double fact_n = Math::fact(n);
//	for (int i = 0; i < ss->size(); i++){
//		tree_counts->push_back(distinct_tree_count(ss->at(i), fact_n, n));
//	}
//	return tree_counts;
//}
//
//double TreeMath::catalan(unsigned int n) {
//	double numerator = Math::fact(2 * n);
//	double denominator = Math::fact(n + 1) * Math::fact(n);
//	return numerator / denominator;
//}
//
//int TreeMath::select_node_count(unsigned int minnode, unsigned int maxnode) {
//	std::unique_ptr<std::vector<double>> catalans(new std::vector<double>);
//	double sum = 0;
//	for (int i = minnode; i <= maxnode; i++) {
//		double cat = catalan(i);
//		catalans->push_back(cat);
//		sum += cat;
//	}
//	return StochasticMath().selprop(catalans, sum) + minnode;
//}
//
//std::shared_ptr<std::vector<int>> TreeMath::select_node_constraint(std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>>& ss, std::unique_ptr<std::vector<double>>& sizes) {
//	int i = StochasticMath().selprop(sizes);
//	return ss->at(i);
//}