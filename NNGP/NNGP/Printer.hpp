#pragma once
#ifndef PRINTER_HPP
#define PRINTER_HPP

#include "AbstractVisitor.hpp"
#include "Nonterminal.hpp"
#include "Terminal.hpp"
#include <iostream>

/*
EXAMPLE USAGE

Nonterminal *bnt= new Nonterminal("double");
Nonterminal *nt = new Nonterminal("triple");
Terminal<int>*t = new Terminal<int>(12);

bnt->behaviour  = [&](Nonterminal* a) { int ret = *((int*)a->vec.at(0)->resolve()) *2; return (void*)&ret; };
nt->behaviour   = [&](Nonterminal* a) { int ret = *((int*)a->vec.at(0)->resolve()) *3; return (void*)&ret; };

bnt->vec.push_back(nt);
nt->vec.push_back(t);

Printer *p = new Printer();
p->visit(bnt);

OUTPUT:
	double
	|    triple
	|    |    12
*/
class Printer : public AbstractVisitor, public std::enable_shared_from_this<Printer>{
private:
	int indent;
	void print_indent();
	void tabright();
	void tableft();
public:
	Printer() { indent = 0; }
	virtual void visit(std::shared_ptr<Nonterminal> a) override;
	virtual void visit(std::shared_ptr<Terminal<float>> a) override;
	virtual void visit(std::shared_ptr<Terminal<glm::vec3>> a) override;
	virtual void visit(std::shared_ptr<Terminal<std::shared_ptr<void>>> a) override;
};

#endif
