#pragma once
#ifndef GPNONTERMINALWRAPPER_HPP
#define GPNONTERMINALWRAPPER_HPP

#include "AbstractNodeWrapper.hpp"
#include "Nonterminal.hpp"

class GPNonterminalWrapper : public AbstractNodeWrapper {
public:
	std::shared_ptr<Nonterminal> t;
	virtual void set_parent(std::shared_ptr<AbstractNode> parent) override {
		this->parent = parent;
		t->set_parent(parent);
	}
	virtual int getdepth() override{
		return this->t->getdepth();
	}
	virtual void set_index(int index) override {
		this->index_as_parameter = index;
		t->set_index(index);
	}
	GPNonterminalWrapper() {
		// for copying
	}
	GPNonterminalWrapper(std::shared_ptr<Behaviour> behaviour, std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> list) {
		this->t = std::make_shared<Nonterminal>();
		this->t->set_behaviour(behaviour);
		this->type = t->type;
		for (int i = 0; i < list->size(); i++) {
			t->parameters->push_back(list->at(i));	// can be made more efficient
		}
	}
	GPNonterminalWrapper(std::shared_ptr<Behaviour> behaviour , std::initializer_list<std::shared_ptr<AbstractNode>> list) {
		this->t = std::make_shared<Nonterminal>();
		this->t->set_behaviour(behaviour);
		this->type = t->type;
		for (int i = 0; i < list.size(); i++) {
			t->parameters->push_back(std::move(list.begin()[i]));	// can be made more efficient
		}
	}
	virtual std::shared_ptr<AbstractNode> copy() override{
		std::shared_ptr<GPNonterminalWrapper> ret = std::make_shared<GPNonterminalWrapper>();
		ret->t = std::static_pointer_cast<Nonterminal>(this->t->copy());
		ret->type = this->type;
		return ret;
	}
	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d) override { return t->resolve(d); }
	virtual void accept(std::shared_ptr<AbstractVisitor> v) override {
		t->accept(v);
	}
};

#endif