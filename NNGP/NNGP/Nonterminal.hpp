#pragma once
#ifndef NONTERMINAL_HPP
#define NONTERMINAL_HPP

#include "AbstractNode.hpp"
#include <iostream>
#include <string>
#include "AbstractVisitor.hpp"
#include "Behaviour.hpp"

/*
Node structures are used to define ASTs in the most compact form possible (1 node to represent both terminals and nonterminals).

EXAMPLE USAGE

Nonterminal *bnt= new Nonterminal("double");
Nonterminal *nt = new Nonterminal("triple");
Terminal<int>*t = new Terminal<int>(12);

bnt->behaviour  = [&](Nonterminal* a) { int ret = *((int*)a->vec.at(0)->resolve()) *2; return (void*)&ret; };
nt->behaviour   = [&](Nonterminal* a) { int ret = *((int*)a->vec.at(0)->resolve()) *3; return (void*)&ret; };

bnt->vec.push_back(nt);
nt->vec.push_back(t);
std::cout << *((int*)bnt->resolve());

THE ANSWER SHOULD BE 12*3*2 = 72
*/

class Nonterminal : public AbstractNode, public std::enable_shared_from_this<Nonterminal>{
public:
	std::shared_ptr<Behaviour> behaviour;
	std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>> parameters;

	virtual void set_parent(std::shared_ptr<AbstractNode> parent) override {
		this->parent = parent;
	}
	virtual int getdepth() override {
		int max = 0;
		for (int i = 0; i < parameters->size(); i++) {
			int pardepth = parameters->at(i)->getdepth();
			if (max < (pardepth + 1)) {
				max = pardepth + 1;
			}
		}
		return max;
	}
	virtual void set_index(int index) override {
		this->index_as_parameter = index;
	}
	void set_behaviour(std::shared_ptr<Behaviour> b) {
		this->behaviour = b;
		this->type = b->type_id;
	}

	Nonterminal() { 
		parameters = std::make_unique<std::vector<std::shared_ptr<AbstractNode>>>(); 
	}

	virtual std::shared_ptr<AbstractNode> copy() override {
		std::shared_ptr<Nonterminal> ret = std::make_shared<Nonterminal>();
		ret->set_index(this->index_as_parameter);
		for (int i = 0; i < this->parameters->size(); i++) {
			std::shared_ptr<AbstractNode> an = this->parameters->at(i)->copy();
			an->type = parameters->at(i)->type;
			an->set_parent(ret);
			ret->parameters->push_back(an);
		}
		ret->set_behaviour(this->behaviour);
		return ret;
	}

	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d) override { return this->behaviour->execute(parameters, d); }

	virtual void accept(std::shared_ptr<AbstractVisitor> v) override { v->visit(shared_from_this()); }
};

#endif
