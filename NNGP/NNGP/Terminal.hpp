#pragma once
#ifndef TERMINAL_HPP
#define TERMINAL_HPP

#include <string>
#include <functional>
#include <vector>
#include "AbstractVisitor.hpp"
#include "AbstractNode.hpp"

template <class T>
class Terminal : public AbstractNode, public std::enable_shared_from_this<Terminal<T>>{
public:
	T val;
	int varid;

	virtual void set_parent(std::shared_ptr<AbstractNode> parent) override {
		this->parent = parent;
	}
	virtual int getdepth() override {
		return 0;
	}
	virtual void set_index(int index) override {
		this->index_as_parameter = index;
	}
	virtual std::shared_ptr<AbstractNode> copy() override{
		std::shared_ptr<Terminal<T>> ret = std::make_shared<Terminal<T>>();
		ret->val = this->val;
		ret->varid = this->varid;
		ret->type = this->type;
		ret->set_index(this->index_as_parameter);
		return ret;
	}

	// Setter-constructor.
	Terminal() {
		// for copying
	}
	Terminal(int type, T val) { 
		this->type = type;
		this->val = val; 
		varid = -1;
	}
	Terminal(int type, std::string anything, int varid) { 
		this->type = type;
		this->varid = varid; 
	}

	// Resolve() acts as a single-dispatcher to execute the behaviour function (even if the only thing you have access to is the base class AbstractNode).
	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d) override {
		if (varid != -1)
			return d->at(varid)->value();
		else
			return std::static_pointer_cast<void>(std::make_shared<T>(val));
	}

	// A handle into the visitor design pattern.
	virtual void accept(std::shared_ptr<AbstractVisitor> v) override { v->visit(shared_from_this()); }
};


// template specialization
template <>
std::shared_ptr<void> Terminal<std::shared_ptr<void>>::resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d){
	if (varid != -1)
		return d->at(varid)->value();
	else
		return val;
}

#endif
