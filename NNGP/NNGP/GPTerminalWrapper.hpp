#pragma once
#ifndef GPTERMINALWRAPPER_HPP
#define GPTERMINALWRAPPER_HPP

#include "AbstractNodeWrapper.hpp"
#include "Terminal.hpp"
#include "Register.hpp"
#include "E_Uninitialized_Terminal.h"

template<class T>
class GPTerminalWrapper : public AbstractNodeWrapper {
public:
	std::shared_ptr<Terminal<T>> t;
	bool initialized;

	virtual void set_parent(std::shared_ptr<AbstractNode> parent) override {
		this->parent = parent;
		t->set_parent(parent);
	}
	virtual int getdepth() override {
		return this->t->getdepth();
	}
	virtual void set_index(int index) override {
		this->index_as_parameter = index;
		t->set_index(index);
	}

	virtual std::shared_ptr<AbstractNode> copy() override {
		std::shared_ptr<GPTerminalWrapper<T>> ret = std::make_shared<GPTerminalWrapper<T>>();
		ret->t = std::static_pointer_cast<Terminal<T>>(this->t->copy());
		ret->initialized = this->initialized;
		ret->type = this->type;
		return ret;
	}

	void set_val(T val) { 
		t->val = val; 
		initialized = true;
	}

	GPTerminalWrapper() {
		// for copying
	}
	GPTerminalWrapper(int type, std::string anything, int varid) {
		this->t = std::make_shared<Terminal<T>>(type, anything, varid);
	}
	GPTerminalWrapper(int type, T val) {
		this->t = std::make_shared<Terminal<T>>(type, val);
	}

	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d) override {
		return t->resolve(d); 
	}
	virtual void accept(std::shared_ptr<AbstractVisitor> v) override { 
		t->accept(v); 
	}
};

#endif