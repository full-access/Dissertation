#ifndef ABSTRACTTREESYNTHESIZER_H
#define ABSTRACTTREESYNTHESIZER_H

#include "AbstractNodeWrapper.hpp"
#include "AbstractType.hpp"

class AbstractTreeSynthesizer {
public:
	virtual std::shared_ptr<AbstractNodeWrapper> synthesize(int depth, std::shared_ptr<AbstractType> type) = 0;
};

#endif