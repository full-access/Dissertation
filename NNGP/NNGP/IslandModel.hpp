#pragma once
#ifndef ISLANDMODEL_HPP
#define ISLANDMODEL_HPP

#include <memory>
#include <vector>
#include "GPLauncher.hpp"
#include "ProgramHandle.hpp"
#include "CommonPool.hpp"
#include <thread>
#include "Cout.hpp"
#include <windows.h>
#include "IntermediateRepresentationTranslator.hpp"

class IslandModel {
public:
	std::shared_ptr<CommonPool> pool;
	std::shared_ptr<std::vector<std::shared_ptr<GPLauncher<float>>>> islands;
	inline static std::shared_ptr<FitnessFunction> ff;

	inline static std::ofstream fitstore;
	inline static std::ofstream irstore;
	inline static std::ofstream attstore;
	inline static std::ofstream msestore;
	inline static std::ofstream szestore;

	inline static bool finished = false;

	//static void run_gp(std::shared_ptr<GPLauncher<float>> gpl, std::shared_ptr<std::vector<std::shared_ptr<ProgramBuffer>>> buffers, std::shared_ptr<bool> timeup, int threadid, float migration_rate) {
	//	while(!(*timeup)){
	//		gpl->run_generation(0.25f, 10, 100);
	//		gpl->broadcast(buffers, migration_rate);
	//		//Cout::cout("\tthread " + std::to_string(threadid) + " completed epoch " + std::to_string(i));
	//	}
	//	Cout::cout("THREAD " + std::to_string(threadid) + " DONE!");
	//}

	IslandModel(std::shared_ptr<FitnessFunction> ff) {
		this->ff = ff;
	}

	static void run_gp(std::shared_ptr<GPLauncher<float>> gpl, std::shared_ptr<ProgramBuffer> buffer, std::shared_ptr<bool> timeup, int threadid) {
		int gens = -1;
		while (!(*timeup)) {
			gens++;
			gpl->run_generation(0.25f, 10, 100);
			if (gens == 9) {
				gpl->broadcast(buffer);
				gens = -1;
			}
		}
		Cout::cout("THREAD " + std::to_string(threadid) + " DONE!");
	}

	static void record(std::shared_ptr<CommonPool> pool, std::shared_ptr<std::vector<std::shared_ptr<GPLauncher<float>>>> islands) {
		std::pair<std::shared_ptr<ProgramHandle>, double> pf;
		std::shared_ptr<ProgramHandle> best;
		double min = -1;
		std::shared_ptr<IntermediateRepresentationTranslator> irt = std::make_shared<IntermediateRepresentationTranslator>();
		int i = 0;
		int index_of_best = -1;
		while (!finished) {
			if (pool->size() == 0) continue;
			min = -1;
			for (int i = 0; i < pool->size(); i++) {
				pf = pool->peek(i);
				if (min == -1 || min > pf.second) {
					min = pf.second;
					index_of_best = i;
					best = pf.first;
				}
			}
			savesamp(min);
			best->accept(irt);

			float pre_attenuation = islands->at(0)->ff->attenuation;
			float attenuation;
			if (std::isinf(min)) {
				for (int i = 0; i < islands->size(); i++) {
					islands->at(i)->ff->attenuation = std::fmax(0.0000001, islands->at(i)->ff->attenuation / 1.01);
					attenuation = islands->at(i)->ff->attenuation;
				}
			}
			else {
				for (int i = 0; i < islands->size(); i++) {
					islands->at(i)->ff->attenuation = std::fmin(islands->at(i)->ff->max_attenuation, islands->at(i)->ff->attenuation * 1.01);
					attenuation = islands->at(i)->ff->attenuation;
				}
			}

			std::string ir = irt->get_and_delete();
			saveir(ir);
			saveatt(pre_attenuation);

			float mse = ff->mse<float>(best, 10000);
			float sze = best->selset->floats->size() + best->selset->vecs->size();

			savemse(mse);
			savesze(sze);
			if (i % 10 == 0) std::cout << "Best fitness: " << min << ", " << mse << ", size " << sze << " and attenuation: " + std::to_string(attenuation) + " with program: " << ir << std::endl;
			if (i % 10 == 0) pool->ff->test_sample(best, 5);
			Sleep(100);
			i++;
		}
	}

	static void savesamp(double sample) {
		if (std::isnan(sample)) {
			fitstore << "NaN";
		}
		else {
			if (std::isinf(sample)) {
				fitstore << "Inf";
			}
			else {
				fitstore << std::to_string(sample);
			}
		}
		fitstore << std::endl;
	}

	static void saveir(std::string ir) {
		irstore << ir << std::endl;
	}

	static void saveatt(float att) {
		if (std::isnan(att)) {
			attstore << "NaN";
		}
		else {
			if (std::isinf(att)) {
				attstore << "Inf";
			}
			else {
				attstore << std::to_string(att);
			}
		}
		attstore << std::endl;
	}
	static void savemse(float mse) {
		if (std::isnan(mse)) {
			msestore << "NaN";
		}
		else {
			if (std::isinf(mse)) {
				msestore << "Inf";
			}
			else {
				msestore << std::to_string(mse);
			}
		}
		msestore << std::endl;
	}
	static void savesze(float sze) {
		if (std::isnan(sze)) {
			szestore << "NaN";
		}
		else {
			if (std::isinf(sze)) {
				szestore << "Inf";
			}
			else {
				szestore << std::to_string(sze);
			}
		}
		szestore << std::endl;
	}


	static void savething(float thing, std::ofstream whatsit) {
		whatsit << thing << std::endl;
	}

	static void countdown(int seconds, std::shared_ptr<bool> timeup) {
		Sleep(seconds * 1000);
		*timeup = true;
	}

	void begin(std::shared_ptr<std::vector<std::shared_ptr<GPLauncher<float>>>> gpls, std::shared_ptr<std::vector<int>> depths, std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> inputs, float migration_rate, std::string fitpath, std::string msepath, std::string szepath, std::string irpath, std::string attpath, int seconds) {

		finished = false;

		pool = std::make_shared<CommonPool>(5, inputs);
		std::shared_ptr<std::vector<std::shared_ptr<ProgramBuffer>>> buffers = std::make_shared<std::vector<std::shared_ptr<ProgramBuffer>>>();
		std::vector<std::thread> threads;

		for (int i = 0; i < gpls->size(); i++) {
			std::cout << "Booting island on thread " << i << std::endl;
			gpls->at(i)->set_pool(pool);
			gpls->at(i)->generate_initial_population(100, depths->at(i));
			buffers->push_back(gpls->at(i)->buffer);
		}

		std::shared_ptr<bool> timeup = std::make_shared<bool>(false);
		for (int is = 0; is < gpls->size(); is++) {
			//threads.push_back(std::thread(run_gp, gpls->at(is), buffers, timeup, is, migration_rate));
			threads.push_back(std::thread(run_gp, gpls->at(is), buffers->at((is + 1) % gpls->size()), timeup, is));
		}
		fitstore.open(fitpath);
		irstore.open(irpath);
		attstore.open(attpath);
		msestore.open(msepath);
		szestore.open(szepath);
		std::thread recorder(record, pool, gpls);
		std::thread stopwatch(countdown, seconds, timeup);

		for (int i = 0; i < threads.size(); i++) {
			threads[i].join();
		}
		finished = true;
		recorder.join();
		stopwatch.join();
		fitstore.close();
		irstore.close();
		attstore.close();
		msestore.close();
		szestore.close();
	}
};

#endif