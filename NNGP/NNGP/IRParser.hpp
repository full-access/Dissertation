#pragma once
#ifndef IRPARSER_HPP
#define IRPARSER_HPP

#include <memory>
#include <iostream>
#include "IRLexer.hpp"
#include "Register.hpp"
#include "AbstractTypeVisitor.hpp"
#include "Type.hpp"
#include <string>
#include "AttributeVector.hpp"
#include "ProgramHandle.hpp"
#include "NodeLogger.hpp"

template <class T>
class IRParser : public AbstractTypeVisitor, public std::enable_shared_from_this<IRParser<T>>{
private:
	IRLexer lexer;
	std::shared_ptr<std::vector<std::string>> format;
	std::shared_ptr<AttributeVector> av;
	bool prog_handle_populated;
	std::shared_ptr<NodeLogger> nl;
	int depth;

	// try create a class that abstracts unordered map functions
	template <typename U>
	int fn_index(std::string s) {
		std::unordered_map<std::string, int>::iterator it;
		it = Register<T>::getinstance()->behaviour_index_lookup->find(s);
		if (it != Register<T>::getinstance()->behaviour_index_lookup->end()) {
			return it->second;
		}
		else {
			return -1;
		}
	}
	template <typename U>
	int cs_index(std::string s) {
		std::unordered_map<std::string, int>::iterator it;
		it = Register<T>::getinstance()->constants_index_lookup->find(s);
		if (it != Register<T>::getinstance()->constants_index_lookup->end()) {
			return it->second;
		}
		else {
			return -1;
		}
	}
public:

	IRParser(std::shared_ptr<std::vector<std::string>> format) {
		this->format = format;
		av = std::make_shared<AttributeVector>(format);
		prog_handle_populated = false;
		nl = std::make_shared<NodeLogger>();
		depth = 0;
	}

	void load_string(std::string ir) {
		prog_handle_populated = false;
		lexer.load_string(ir);
	}

	std::shared_ptr<AttributeVector> get_and_delete_attrib_vec() {
		prog_handle_populated = false;
		std::shared_ptr<AttributeVector> temp = av;
		av = std::make_shared<AttributeVector>(format);
		//std::shared_ptr<AttributeVector> temp = std::make_shared<AttributeVector>(format);
		//(*(temp->attrib_freqs))["depth"] = depth;
		depth = 0;
		return temp;
	}


	std::shared_ptr<void> parse(){
		std::shared_ptr<Type<T>> type = std::make_shared<Type<T>>();
		std::shared_ptr<void> ret = type->accept(shared_from_this(), 0, false);
		//depth = std::static_pointer_cast<ProgramHandle>(ret)->getdepth();
		return ret;
	}

	std::shared_ptr<void> parse_float() {
		
		bool attach_handle = !prog_handle_populated;
		if (!prog_handle_populated) prog_handle_populated = true;

		std::string token = "";
		token = lexer.next();

		std::string id = "none";

		// consume redundant tokens (e.g. 2 microlexemes (like ')' and ',') next to each other, or types)
		bool cond = true;
		while (cond) {
			cond = false;
			if (token == "float" || token == "vec3" || token == "") { 
				if (token == "float" || token == "vec3") id = token;
				token = lexer.next();
				cond = true;
			}
		}

		// if it's a function...
		if (id == "none") {
			int index = fn_index<T>(token);
			std::shared_ptr<GPBehaviourWrapper<T>> node = Register<T>::getinstance()->behaviours->at(index);
			this->av->incr_attrib(node->displayname);
			std::vector<std::shared_ptr<AbstractType>> paramtypes = node->paramtypes;
			std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> list = std::make_shared<std::vector<std::shared_ptr<AbstractNode>>>();
			for (int i = 0; i < paramtypes.size(); i++) {
				list->push_back(std::static_pointer_cast<AbstractNode>(paramtypes.at(i)->accept(shared_from_this(), 0, false)));
			}
			std::shared_ptr<GPNonterminalWrapper> ret = std::make_shared<GPNonterminalWrapper>(node, list);
			// setting parents
			for (int i = 0; i < list->size(); i++) {
				list->at(i)->set_parent(ret);
				list->at(i)->set_index(i);
			}
			if (attach_handle) {
				std::shared_ptr<ProgramHandle> prog = std::make_shared<ProgramHandle>(ret);
				ret->set_parent(prog);
				prog->accept(nl);
				prog->selset = nl->get_and_delete();
				return std::static_pointer_cast<void>(prog);
			}
			return std::static_pointer_cast<void>(ret);
		}

		// if it's a terminal...
		else {

			int type = 0;
			if (id == "float") type = 0;
			if (id == "vec3") type = 1;

			// if it's a variable (only supports P[0..9] so far...)
			if (token[0] == 'P') {
				std::shared_ptr<GPTerminalWrapper<T>> ret = std::make_shared<GPTerminalWrapper<T>>(type, "argv", (int)token[2] - 48);
				if (attach_handle) {
					std::shared_ptr<ProgramHandle> prog = std::make_shared<ProgramHandle>(ret);
					ret->set_parent(prog);
					prog->accept(nl);
					prog->selset = nl->get_and_delete();
					id = "none";
					return std::static_pointer_cast<void>(prog);
				}
				id = "none";
				return std::static_pointer_cast<void>(ret);
			}

			// if it's a constant...
			if (id == "vec3") {
				token = "("+token+","+lexer.next()+","+lexer.next()+")";
			}
			int index = cs_index<T>(token);
			if (index > -1) {
				std::shared_ptr<GPTerminalWrapper<T>> ret = std::make_shared<GPTerminalWrapper<T>>(type, *Register<T>::getinstance()->constants->at(index));
				if (attach_handle) {
					std::shared_ptr<ProgramHandle> prog = std::make_shared<ProgramHandle>(ret);
					ret->set_parent(prog);
					prog->accept(nl);
					prog->selset = nl->get_and_delete();
					id = "none";
					return std::static_pointer_cast<void>(prog);
				}
				id = "none";
				return std::static_pointer_cast<void>(ret);
			}
			else {
				// problem
			}			
		}
	}


	std::shared_ptr<void> parse_vec3() {
		return parse_float();
	}

	// VERY TACKY... AUGHT TO RETHINK
	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<float>>, int depth, bool grow) override {
		return parse_float();
	}
	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<glm::vec3>>, int depth, bool grow) override {
		return parse_vec3();
	}
};

#endif
