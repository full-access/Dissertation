#pragma once
#ifndef TYPE_HPP
#define TYPE_HPP

#include "AbstractType.hpp"

/*
The type class exists for type comparison and storage.
*/
template <class T>
class Type : public AbstractType, public std::enable_shared_from_this<Type<T>>{
public:
	virtual std::shared_ptr<void> accept(std::shared_ptr<AbstractTypeVisitor> visitor, int depth, bool grow) override { return visitor->visit(shared_from_this(), depth, grow); }
};

#endif