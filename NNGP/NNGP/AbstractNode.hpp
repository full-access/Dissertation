#pragma once
#ifndef ABSTRACTNODE_HPP
#define ABSTRACTNODE_HPP

#include "AbstractVisitor.hpp"
#include "AbstractDatum.hpp"
#include <vector>
#include <memory>

/*
Abstract class for all node types including terminals and nonterminals.
*/
class AbstractNode{
public:
	int type;	// this is a temporary fix...

	std::weak_ptr<AbstractNode> parent;
	int index_as_parameter = 0;
	virtual void set_parent(std::shared_ptr<AbstractNode> parent) = 0;
	virtual void set_index(int index) = 0;

	virtual int getdepth() = 0;

	virtual std::shared_ptr<AbstractNode> copy() = 0;

	// The apply function pointer is set to the behaviour of the instance at runtime. Correct usage - somenode->apply  = [&](Node* a) { int ret = (int)a->vec.at(0)->apply(a->vec.at(0)) * 2; return (void*)ret; };
	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>) = 0;
	
	// A handle into the visitor design pattern.
	virtual void accept(std::shared_ptr<AbstractVisitor> v) = 0;
};

#endif
