#pragma once
#ifndef BEHAVIOUR_HPP
#define BEHAVIOUR_hpp

#include <functional>
#include <memory>
#include <vector>
#include "AbstractNode.hpp"
#include "AbstractType.hpp"

/*
A storage class for a function pointer.

Behaviour objects are instantiated once per specific functionality. Once a node wishes to use the functionality described in one of these instantiated behaviours, it simply uses a reference to
the object. This allows for multiple nodes using the exact same function without having to store duplicates in memory. Moreover, since these function pointers are dynamically bound at runtime,
the programmer can define an entire library in a single document (with the expense of some runtime).
*/
class Behaviour{
public:
	// The displayname serves as both an identifier for debugging, as well as a c++ translation mechanism
	// eg. a square-root function would be translated to std::sqrt(operand), hence this will be the display name
	// it is also possible to use the INFIX keyword to place the displayname in between two operands
	// eg. INFIX+ means (operand1 + operand2). Note that the INFIX keyword currently only supports 1 character after it
	std::string displayname;
	// Arity is the number of arguments the behaviour accepts.
	int arity;
	int type_id;	// temporary fix...
	std::shared_ptr<AbstractType> type;
	Behaviour(int arity) { this->arity = arity; }
	
	// The behaviour function pointer is set to the behaviour of the instance at runtime. Correct usage - somenonterminal->apply  = [&](Nonterminal* a) { int ret = (int)a->vec.at(0)->resolve() * 2; return (void*)ret; };
	std::function<std::shared_ptr<void>(std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>&, std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>&)> execute;
};

#endif