#pragma once
#ifndef IRPOPASTRECONSTRUCTOR_HPP
#define IRPOPASTRECONSTRUCTOR_HPP

#include "AbstractNode.hpp"
#include "IRParser.hpp"
#include <iostream>
#include <memory>
#include "AttributeVector.hpp"

class IRPopulationASTReconstructor {
private:
	std::shared_ptr<std::vector<std::shared_ptr<AttributeVector>>> avs;
	std::shared_ptr<std::vector<std::string>> format;
public:
	IRPopulationASTReconstructor(std::shared_ptr<std::vector<std::string>> format) {
		this->format = format;
		avs = std::make_shared<std::vector<std::shared_ptr<AttributeVector>>>();
	}
	std::shared_ptr<std::vector<std::shared_ptr<AttributeVector>>> get_and_delete_attrib_vecs() {
		std::shared_ptr<std::vector<std::shared_ptr<AttributeVector>>> temp = avs;
		avs = std::make_shared<std::vector<std::shared_ptr<AttributeVector>>>();
		return temp;
	}
	std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> reconstruct(std::shared_ptr<std::vector<std::string>> irs) {
		std::cout << "Reconstructing " << irs->size() << " IRs." << std::endl;
		std::shared_ptr<IRParser<float>> irp = std::make_shared<IRParser<float>>(format);

		std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> progs = std::make_shared<std::vector<std::shared_ptr<AbstractNode>>>();
		for (int i = 0; i < irs->size(); i++) {
			irp->load_string(irs->at(i));
			progs->push_back(std::static_pointer_cast<AbstractNode>(irp->parse()));
			avs->push_back(irp->get_and_delete_attrib_vec());
		}
		std::cout << "Reconstruction complete." << std::endl;
		return progs;
	}
};

#endif