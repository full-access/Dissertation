#pragma once
#ifndef COUT_HPP
#define COUT_HPP

#include <iostream>
#include <string>
#include <mutex>

static class Cout {
public:
	static inline std::mutex mut;
	static void cout(std::string s) {
		mut.lock();
		std::cout << s << std::endl;
		mut.unlock();
	}
};

#endif