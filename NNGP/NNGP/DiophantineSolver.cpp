#include <iostream>
#include <memory>
#include <vector>
#include "DiophantineSolver.hpp"

std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>> DiophantineSolver::solve(std::unique_ptr<std::vector<int>>& arity_constraint, int rhs) {
	std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>> node_constraint(new std::vector<std::shared_ptr<std::vector<int>>>);
	std::shared_ptr<std::vector<int>> accumulator(new std::vector<int>);
	solve_helper(arity_constraint, arity_constraint->size() - 1, rhs, node_constraint, accumulator);	// bootstrap
	return node_constraint;
}

void DiophantineSolver::solve_helper(std::unique_ptr<std::vector<int>>& arity_constraint,	// a list of all the function arities (eg. {1,2,3,4})
	int index,	// this index starts off pointing to the rightmost value in arities (3 in this example)
	int rhs,	// the rhs of the diophantine equation
	std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>>& node_constraint,	// the output
	std::shared_ptr<std::vector<int>> accumulator) {	// an accumulator for recursion to work

	// base case
	if (index == -1) {
		if (rhs == 0) node_constraint->push_back(accumulator);
		return;
	}

	int largest_arity = arity_constraint->at(index);
	int maxdivs = rhs / largest_arity;

	// deep copy of the accumulator
	for (int i = 0; i <= maxdivs; i++) {
		std::shared_ptr<std::vector<int>> copy(new std::vector<int>);
		for (int j = 0; j < accumulator->size(); j++) {
			copy->push_back(accumulator->at(j));
		}
		copy->push_back(maxdivs - i);

		// run the function again, with slightly different parameters
		// once the base case is reached, dump the accumulator (now called copy) value in the node_constraints and carry on
		solve_helper(arity_constraint, index - 1, rhs - (largest_arity*(maxdivs - i)), node_constraint, copy);
	}
}

