#include <cstdlib>
#include <random>
#include <chrono>
#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include "StochasticMath.hpp"
#include <mutex>

static std::mt19937 eng{ static_cast<std::mt19937::result_type>(std::chrono::high_resolution_clock::now().time_since_epoch().count()) };
static std::mutex mut;

int StochasticMath::randint(int min, int max) {
	mut.lock();
	int ret = std::uniform_int_distribution<>(min, max)(eng);
	mut.unlock();
	return ret;
}

void StochasticMath::sort(std::shared_ptr<std::vector<std::pair<double, int>>> list){
	std::sort(list->begin(), list->end(), compare_tuples());
}

int StochasticMath::ranksel(int count) {
	double rand = randint(0, 1000000) / 1000000.0;
	float tempsum = (count / (float)2)*(1 + count);
	double selection_point = rand * tempsum;	
	for (int i = count; i > 0; i--) {
		tempsum -= i;
		if (tempsum < selection_point) return i-1;
	}
}

// Uses roulette-wheel style of selection.
unsigned int StochasticMath::selprop(std::shared_ptr<std::vector<double>>& list, double sumlist) {
	double rand = randint(0, 1000000) / (double)1000000.0;
	double selection_point = rand * sumlist;
	double tempsum = 0;
	for (int i = 0; i < list->size(); i++){
		tempsum += list->at(i);
		if (list->at(i) != 0 && tempsum >= selection_point)
			return i;
	}
}

unsigned int StochasticMath::selprop(std::initializer_list<float> list){
	std::shared_ptr<std::vector<double>> vec = std::make_shared<std::vector<double>>();
	float total = 0;
	for (float t : list) {
		total += t;
		vec->push_back((double)t);
	}
	return selprop(vec, total);
}

unsigned int StochasticMath::selprop(std::shared_ptr<std::vector<double>>& list) {
	double sumlist = 0;
	for (int i = 0; i < list->size(); i++) {
		sumlist += list->at(i);
	}
	return selprop(list, sumlist);
}

unsigned int StochasticMath::selprop(std::unique_ptr<std::vector<int>>& list, double sumlist) {
	double rand = randint(0, 1000) / (double)1000.0;
	double selection_point = rand * sumlist;
	int tempsum = 0;
	for (int i = 0; i < list->size(); i++) {
		tempsum += list->at(i);
		if (list->at(i) != 0 && tempsum >= selection_point)
			return i;
	}
}

unsigned int StochasticMath::selprop(std::unique_ptr<std::vector<int>>& list) {
	double sumlist = 0;
	for (int i = 0; i < list->size(); i++) {
		sumlist += list->at(i);
	}
	return selprop(list, sumlist);
}

void StochasticMath::shuffle(std::unique_ptr<std::vector<int>>& input){
	std::shuffle(std::begin(*input), std::end(*input), eng);
}
