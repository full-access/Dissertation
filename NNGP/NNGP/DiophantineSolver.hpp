#ifndef DIOPHANTINE_H
#define DIOPHANTINE_H

// A class that solves the diophantine equation for tree synthesis
// Note that this was only designed to work with very small arity constraints (ideal for BRDF generation)
static class DiophantineSolver {
public:
	// Bootstrapper to the actual recursive function.
	static std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>> solve(std::unique_ptr<std::vector<int>>& arity_constraint, int rhs);

private:
	/*
	Pseudocode:
	1. start off with a stack of arities (the largest value at the top), an rhs to the dioph. equation, the output stack and a temporary stack.
	2. BASECASE (see later for this to make sense...)
	3. t = arities.pop (the top value gets removed from arities)
	4. r = truncate(rhs / t) this is done to find the maximum number of times the largest element in the dioph. eq. fits into the rhs.

	For example, arities = {1,2,3} and rhs = 8, 3 fits twice in 8. so let's assume 1x + 2y + 3(2) = 8
	This can be reduced to 1x + 2y = 2. Notice how this is yet another dioph. equation? simply run the function again over this... (recusively)

	If we run the above code, we'll only end up solving for maximum z, y, and x in that order.
	We need a for loop to decrement z, y, and x through all possible values (but we know z can't be larger than 2)

	5. push r into the temp stack
	6. repeat the function, with the reduced formula.
	7. repeat from step 3 onwards, but now decrement r by 1.

	BASECASE = if the arities stack is empty, dump the temp stack into the output stack and return from the recursion branch.

	Examples:
	arities = {1,2,3,4} and rhs = 4: (0 0 0 1), (1 0 1 0), (0 2 0 0), (2 1 0 0), (4 0 0 0) [Iba1996]
	*/
	static void solve_helper(std::unique_ptr<std::vector<int>>& arity_constraint,
		int index,
		int rhs,
		std::unique_ptr<std::vector<std::shared_ptr<std::vector<int>>>>& node_constraint,
		std::shared_ptr<std::vector<int>> accumulator);
};

#endif