#pragma once
#ifndef TEXTTOARGUMENTPARSER_HPP
#define TEXTTOARGUMENTPARSER_HPP

#include "AbstractDatum.hpp"
#include <memory>
#include <glm\glm.hpp>
#include <string>
#include "Datum.hpp"
#include <vector>

class TextToArgumentParser {
public:
	std::string parse_real(std::string text, int& ind) {
		std::string ret = "";
		if (text[ind] == ';' || text[ind] == '\n') ind++;
		if (ind >= text.size()) {
			ind = -1;
			return ret;
		}
		while (text[ind] != ';' && text[ind] != '\n' && ind < text.size()) ret += text[ind++];
		return ret;
	}

	glm::vec3 parse_vec(std::string vec) {
		float pre[3];
		int ind = 0;
		for (int i = 0; i < 3; i++) {
			std::string temp = "";
			while (ind < vec.size() && vec[ind] != ',') {
				temp += vec[ind++];
			}
			ind++;
			pre[i] = std::stof(temp);
			temp = "";
		}
		return glm::vec3(pre[0], pre[1], pre[2]);
	}

	std::shared_ptr<AbstractDatum> parse_datum(std::string datum, bool& isfloat) {
		isfloat = datum.find(',') == std::string::npos;
		std::shared_ptr<AbstractDatum> ret;
		if (isfloat) return std::make_shared<Datum<float>>(std::stof(datum));
		return std::make_shared<Datum<glm::vec3>>(parse_vec(datum));
	}

	std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> parse_argument_list(std::string text, int& ind, std::shared_ptr<std::vector<bool>> format) {
		std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> data = std::make_shared<std::vector<std::shared_ptr<AbstractDatum>>>();
		while (true) {
			std::string temp = parse_real(text, ind);
			if (ind == -1) break;
			bool isfloat;
			data->push_back(parse_datum(temp, isfloat));
			if(format != nullptr) format->push_back(isfloat);
			if (text[ind] == '\n') {
				ind++;
				break;
			}
		}
		return data;
	}

	std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> parse(std::string text, std::shared_ptr<std::vector<bool>>& format){
		std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> datas = std::make_shared<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>>();
		std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> data = std::make_shared<std::vector<std::shared_ptr<AbstractDatum>>>();
		std::shared_ptr<std::vector<bool>> temp = std::make_shared<std::vector<bool>>();
		int ind = 0;
		while (true) {
			auto datum = parse_argument_list(text, ind, temp);
			if (temp != nullptr) {
				format = temp;
				temp = nullptr;
			}
			if (!datum->empty()) {
				datas->push_back(datum);
			}else {
				break;
			}
			if (ind == -1) break;
		}
		return datas;
	}

};

#endif