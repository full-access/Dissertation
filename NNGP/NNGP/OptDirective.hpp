#pragma once
#ifndef OPTDIR_HPP
#define OPTDIR_HPP

#include <boost/regex.hpp>

class OptDirective {
private:
	boost::regex regex;
	std::string replacement;
public:
	OptDirective(std::string regex, std::string replacement) {
		this->regex = boost::regex{ regex };
		this->replacement = replacement;
	}
	std::string match_and_replace(std::string input) {
		return boost::regex_replace(input, this->regex, this->replacement);
	}
};

#endif