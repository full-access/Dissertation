#ifndef INTREP_H
#define INTREP_H

class Nonterminal;
#include "AbstractVisitor.hpp"
#include <iostream>

class IntermediateRepresentationTranslator : public AbstractVisitor, public std::enable_shared_from_this<IntermediateRepresentationTranslator>{
private:
	std::string intermediate_rep;
public:
	std::shared_ptr<std::vector<std::string>> saver;
	IntermediateRepresentationTranslator() {
		saver = std::make_shared<std::vector<std::string>>();
	}
	std::string get_and_delete() {
		std::string temp = intermediate_rep;
		saver->push_back(temp);
		intermediate_rep = "";
		return temp;
	}
	virtual void visit(std::shared_ptr<Nonterminal> a) override {
		intermediate_rep += a->behaviour->displayname + "(";
		for (int i = 0; i < a->parameters->size(); i++) {
			a->parameters->at(i)->accept(shared_from_this());
			if (i < a->parameters->size() - 1) intermediate_rep += ",";
		}
		intermediate_rep += ")";
	}
	virtual void visit(std::shared_ptr<Terminal<float>> a) override {
		if (a->varid > -1)
			intermediate_rep += "float(P["+std::to_string(a->varid)+"])";
		else
			intermediate_rep += "float(" + std::to_string(a->val) + ")";
	}
	virtual void visit(std::shared_ptr<Terminal<glm::vec3>> a) override {
		if (a->varid > -1)
			intermediate_rep += "vec3(P[" + std::to_string(a->varid) + "])";
		else
			intermediate_rep += "vec3(" + std::to_string(a->val.x) + "," + std::to_string(a->val.y) + "," + std::to_string(a->val.z) + ")";
	}
	virtual void visit(std::shared_ptr<Terminal<std::shared_ptr<void>>> a) override {}
};

#endif