#ifndef DYCK_H
#define DYCK_H

// Maps tree configurations into dyck words.
static class DyckWordMapper {
public:
	static std::string map(const std::unique_ptr<std::vector<int>>& arities, const std::shared_ptr<std::vector<int>> configuration, int node_count);
private:
	// Converts a number into an equivalent number of "y"s.
	static std::string num_to_y(int n);
	// Converts a list (eg. 10210) into a dyck word (xyxxyyxyx).
	static std::string to_dyck(const std::unique_ptr<std::vector<int>>& in);
	// Packs an intermediate configuration with 0s to fit a node count.
	static void pack(std::unique_ptr<std::vector<int>>& in, int node_count, int nodes_added);
	// Unravels a configuration into its constituent parts (eg. 3,2,0,1 -> 1,1,1,2,2,4).
	static void unravel(const std::unique_ptr<std::vector<int>>& arities, const std::shared_ptr<std::vector<int>>& configuration, std::unique_ptr<std::vector<int>>& out, int node_count);
};

#endif