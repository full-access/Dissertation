#pragma once
#ifndef PROGRAMHANDLE_HPP
#define PROGRAMHANDLE_HPP

#include "AbstractNode.hpp"
#include "AbstractVisitor.hpp"
#include "SelectionSet.hpp"
#include "NodeLogger.hpp"

class ProgramHandle: public AbstractNode{
public:
	std::shared_ptr<AbstractNode> child;
	std::shared_ptr<SelectionSet> selset;

	ProgramHandle(std::shared_ptr<AbstractNode> child) {
		this->type = child->type;
		this->child = child;
	}
	virtual int getdepth() override {
		return child->getdepth();
	}
	virtual std::shared_ptr<AbstractNode> copy() override {
		std::shared_ptr<ProgramHandle> ret = std::make_shared<ProgramHandle>(this->child->copy());
		ret->type = this->type;
		ret->child->set_parent(ret);
		return ret;
	}
	std::shared_ptr<AbstractNode> copy(std::shared_ptr<NodeLogger> nl) {
		std::shared_ptr<ProgramHandle> ret = std::static_pointer_cast<ProgramHandle>(this->copy());
		ret->accept(nl);
		ret->selset = nl->get_and_delete();
		return ret;
	}

	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d) override { return child->resolve(d); }
	virtual void set_parent(std::shared_ptr<AbstractNode> parent) override {	}
	virtual void set_index(int index) override {	}
	virtual void accept(std::shared_ptr<AbstractVisitor> v) override { child->accept(v); }
};

#endif
