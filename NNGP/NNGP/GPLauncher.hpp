#pragma once
#ifndef GPLAUNCHER_HPP
#define GPLAUNCHER_HPP

#include "RHHSynthesizer.h"
#include "CrossoverTools.hpp"
#include "GPTools.hpp"
#include "ProgramHandle.hpp"
#include "FitnessFunction.hpp"
#include "NodeLogger.hpp"
#include "CppTranslator.hpp"
#include <memory>
#include <algorithm>
#include "StochasticMath.hpp"
#include "ProgramBuffer.hpp"
#include "CommonPool.hpp"
#include "StochasticMath.hpp"
#include "Cout.hpp"

template <typename T>
class GPLauncher {
public:
	std::shared_ptr<ProgramBuffer> buffer;
	std::shared_ptr<CommonPool> pool;
	std::shared_ptr<FitnessFunction> ff;
	std::shared_ptr<NodeLogger> nl;
	std::shared_ptr<RHHSynthesizer> rhh;

	std::shared_ptr<std::vector<std::shared_ptr<ProgramHandle>>> current_generation;
	std::shared_ptr<std::vector<std::pair<double, int>>> fitnesses;

	double best_fit;
	int best_fit_index;

	int samp_size;
	int island_id;

	GPLauncher(std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>>>> inputs, int samp_size, int island_id){
		this->buffer = std::make_shared<ProgramBuffer>(5);
		this->ff = std::make_shared<FitnessFunction>(inputs);
		nl = std::make_shared<NodeLogger>();
		rhh = std::make_shared<RHHSynthesizer>(island_id);
		this->pool = nullptr;
		fitnesses = std::make_shared<std::vector<std::pair<double, int>>>();
		this->samp_size = samp_size;
		this->island_id = island_id;
	}

	void set_pool(std::shared_ptr<CommonPool> pool) {
		this->pool = pool;
	}

	// CALL THIS BEFORE STARTING THE GP RUN !!
	void generate_initial_population(int count, int maxdepth) {
		std::shared_ptr<std::vector<std::shared_ptr<ProgramHandle>>> ret = std::make_shared<std::vector<std::shared_ptr<ProgramHandle>>>();
		for (int i = count; i > 0; i--) {
			int depth = std::ceil((float)(maxdepth*(i / (float)count)));
			bool grow = (i % 2 == 0) ? true : false;
			ret->push_back(std::static_pointer_cast<ProgramHandle>(rhh->force_accept(std::make_shared<Type<T>>(), depth, grow, true)));
		}
		current_generation = ret;

		// calculating fitnesses of initial generation
		fitnesses->clear();
		best_fit = 0;
		best_fit_index = 0;
		for (int i = 0; i < current_generation->size(); i++) {
			double fitness = 0;
			fitness = ff->fitness<T>(current_generation->at(i), samp_size);
			fitnesses->push_back(std::make_pair(fitness, i));
			if (!std::isnan(fitness) && (i == 0 || best_fit > fitness)) {
				best_fit = fitness;
				best_fit_index = i;
			}
		}
	}

	void run_generation(float mutchance, int broodsize, int ppg) {
		
		std::shared_ptr<ProgramHandle> ph;
		while ((ph = buffer->pop()) != nullptr) {
			current_generation->push_back(ph);
		}
		//std::shared_ptr<CppTranslator> cpp = std::make_shared<CppTranslator>("ph");
		
		//current_generation->at(best_fit_index)->accept(cpp);
		//std::cout << cpp->get_and_delete() << std::endl;
		//std::cout << "DEPTH: " << current_generation->at(best_fit_index)->getdepth() << std::endl;
		//std::cout << "FIT: " << best_fit << std::endl;

		if(pool != nullptr)
			report(std::static_pointer_cast<ProgramHandle>(current_generation->at(best_fit_index)->copy(nl)), best_fit);
		
		// elitism
		std::shared_ptr<std::vector<std::shared_ptr<ProgramHandle>>> next_generation = std::make_shared<std::vector<std::shared_ptr<ProgramHandle>>>();
		next_generation->push_back(std::static_pointer_cast<ProgramHandle>(current_generation->at(best_fit_index)->copy(nl)));
		
		StochasticMath().sort(fitnesses);

		// generating new generation
		for (int i = 0; i < ppg-1; i++) {
			int rand1 = StochasticMath().ranksel(fitnesses->size());
			int rand2 = StochasticMath().ranksel(fitnesses->size());
			int sel1 = fitnesses->at(rand1).second;
			int sel2 = fitnesses->at(rand2).second;
			std::shared_ptr<ProgramHandle> ast1 = current_generation->at(sel1);
			std::shared_ptr<ProgramHandle> ast2 = current_generation->at(sel2);
			std::shared_ptr<std::vector<std::shared_ptr<ProgramHandle>>> brood = std::make_shared<std::vector<std::shared_ptr<ProgramHandle>>>();
			
			// generating brood
			std::shared_ptr<ProgramHandle> ast1_copy;
			std::shared_ptr<ProgramHandle> ast2_copy;
			for (int j = 0; j < broodsize; j++) {
				ast1_copy = std::static_pointer_cast<ProgramHandle>(ast1->copy(nl));
				ast2_copy = std::static_pointer_cast<ProgramHandle>(ast2->copy(nl));

				if (CrossoverTools(island_id).crossover(ast1_copy, ast2_copy) == -1) {
					//Cout::cout("NO CROSSOVER POSSIBLE");
					GPTools(island_id).mutate(ast1_copy);
					GPTools(island_id).mutate(ast2_copy);
				}
				else {
					if (StochasticMath().randint(0, 10000) / ((float)(10000)) < mutchance) {
						GPTools(island_id).mutate(ast1_copy);
					}
					if (StochasticMath().randint(0, 10000) / ((float)(10000)) < mutchance) {
						GPTools(island_id).mutate(ast2_copy);
					}
				}
				brood->push_back(ast1_copy);
				brood->push_back(ast2_copy);
			}

			// calculating fitnesses of brood
			double best_fit = 0;
			int best_fit_index = 0;
			for (int j = 0; j < broodsize; j++) {
				double fitness = 0;
				fitness = ff->fitness<T>(brood->at(j), samp_size);
				if (j == 0 || best_fit > fitness) {
					best_fit = fitness;
					best_fit_index = j;
				}
			}

			// select the best member of the brood
			next_generation->push_back(std::static_pointer_cast<ProgramHandle>(brood->at(best_fit_index)->copy(nl)));
		}
		// set up for the next iteration
		current_generation = next_generation;

		// calculating fitnesses for the next iteration of the run
		fitnesses->clear();
		best_fit = 0;
		best_fit_index = 0;
		for (int i = 0; i < current_generation->size(); i++) {
			double fitness = 0;
			fitness = ff->fitness<T>(current_generation->at(i), samp_size);
			fitnesses->push_back(std::make_pair(fitness, i));
			if (i == 0 || best_fit > fitness) {
				best_fit = fitness;
				best_fit_index = i;
			}
		}
	}

	void report(std::shared_ptr<ProgramHandle> p, double fitness) {
		pool->push(std::pair<std::shared_ptr<ProgramHandle>, double>(p, fitness));
	}

	//void broadcast(std::shared_ptr<std::vector<std::shared_ptr<ProgramBuffer>>> buffers, float migration_chance) {
	//	for (int i = 0; i < buffers->size(); i++) {
	//		ProgramBuffer * thisbuf = this->buffer.get();
	//		ProgramBuffer * thatbuf = buffers->at(i).get();
	//		if (thisbuf == thatbuf) {
	//			continue;
	//		}
	//		float rand = StochasticMath().randint(0, 1000) / (float)(1000);
	//		if (migration_chance >= rand) {
	//			//Cout::cout("MIGRATION");
	//			int sel = fitnesses->at(StochasticMath().ranksel(fitnesses->size())).second;
	//			std::shared_ptr<ProgramHandle> ast = std::static_pointer_cast<ProgramHandle>(current_generation->at(sel)->copy(nl));
	//			buffers->at(i)->push(ast);
	//		}
	//	}
	//}

	void broadcast(std::shared_ptr<ProgramBuffer> buffer) {
		buffer->push(this->current_generation->at(best_fit_index));
	}
};

#endif