#pragma once
#ifndef GPTOOLS_HPP
#define GPTOOLS_HPP

#include "SelectionSet.hpp"
#include "AbstractNode.hpp"
#include "StochasticMath.hpp"
#include "RHHSynthesizer.h"
#include "NodeLogger.hpp"
#include <memory>
#include <algorithm> 

class GPTools {
public:
	std::shared_ptr<RHHSynthesizer> rhh;
	std::shared_ptr<NodeLogger> nl;

	GPTools(int island_id){
		rhh = std::make_shared<RHHSynthesizer>(island_id);
		nl = std::make_shared<NodeLogger>();
	}

	void unite(std::shared_ptr<SelectionSet> to, std::shared_ptr<SelectionSet> from) {
		to->floats->insert(from->floats->begin(), from->floats->end());
		to->vecs->insert(from->vecs->begin(), from->vecs->end());
	}
	void difference(std::shared_ptr<SelectionSet> to, std::shared_ptr<SelectionSet> from) {
		std::set<std::shared_ptr<AbstractNode>> floatret;
		std::set<std::shared_ptr<AbstractNode>> vecret;
		std::set_difference(to->floats->begin(), to->floats->end(), from->floats->begin(), from->floats->end(), std::inserter(floatret, floatret.end()));
		std::set_difference(to->vecs->begin(), to->vecs->end(), from->vecs->begin(), from->vecs->end(), std::inserter(vecret, vecret.end()));
		to->floats = std::make_shared<std::set<std::shared_ptr<AbstractNode>>>(floatret);
		to->vecs = std::make_shared<std::set<std::shared_ptr<AbstractNode>>>(vecret);
	}
	std::shared_ptr<AbstractNode> select_random_node(std::shared_ptr<SelectionSet> set) {
		std::shared_ptr<std::set<std::shared_ptr<AbstractNode>>> selection;
		std::unique_ptr<std::vector<int>> list = std::make_unique<std::vector<int>>();

		int floatsize = set->floats->size();
		int vecsize = set->vecs->size();

		list->push_back(floatsize);
		list->push_back(vecsize);

		unsigned int cond = StochasticMath().selprop(list, floatsize + vecsize);
		
		if (cond == 0) {
			selection = set->floats;
		}else {
			selection = set->vecs;
		}

		std::set<std::shared_ptr<AbstractNode>>::const_iterator it(selection->begin());
		advance(it, StochasticMath().randint(0, selection->size()-1));
		return *it;
	}

	void mutate(std::shared_ptr<ProgramHandle> a) {
		std::shared_ptr<SelectionSet> set = a->selset;
		std::shared_ptr<AbstractNode> node = select_random_node(set);
		int depth = node->getdepth();
		int type = node->type;
		std::shared_ptr<ProgramHandle> mutnode;
		if (type == 0)
			mutnode = std::static_pointer_cast<ProgramHandle>(rhh->force_accept(std::make_shared<Type<float>>(), depth, true, true));
		else if (type == 1)
			mutnode = std::static_pointer_cast<ProgramHandle>(rhh->force_accept(std::make_shared<Type<glm::vec3>>(), depth, true, true));
		else std::cerr << "problem" << std::endl;

		std::shared_ptr<SelectionSet> ss = mutnode->selset;

		if (node->parent.lock()->parent.lock() == nullptr) {
			std::shared_ptr<ProgramHandle> nodeparent = std::static_pointer_cast<ProgramHandle>(node->parent.lock());
			nodeparent->child = mutnode->child;
			nodeparent->selset = ss;
			mutnode->child->set_index(0);			
		}
		else {
			std::shared_ptr<GPNonterminalWrapper> nodeparent = std::static_pointer_cast<GPNonterminalWrapper>(node->parent.lock());
			nodeparent->t->parameters->at(node->index_as_parameter) = mutnode->child;
			node->accept(nl);
			std::shared_ptr<SelectionSet> removeme = nl->get_and_delete();
			GPTools::difference(a->selset, removeme);
			GPTools::unite(a->selset, ss);
			mutnode->child->set_index(node->index_as_parameter);
		}
		mutnode->child->parent = node->parent;
	}
};

#endif