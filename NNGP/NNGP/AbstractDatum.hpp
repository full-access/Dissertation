#ifndef ABSTRACTDATUM_H
#define ABSTRACTDATUM_H

#include <memory>

class AbstractDatum {
public:
	virtual std::shared_ptr<void> value() = 0;
};

#endif 