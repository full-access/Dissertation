#pragma once
#ifndef ABSTRACTNODEWRAPPER_HPP
#define ABSTRACTNODEWRAPPER_HPP

#include "AbstractNode.hpp"

/*
Allows for the injection of added functionality to existing AST base classes:

std::shared_ptr<Behaviour> vec3_doubler(new Behaviour(1));
vec3_doubler->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a) { glm::vec3 ret = *((glm::vec3*)a->at(0)->resolve()) * 2.0f; return (void*)&ret; };
std::shared_ptr<Behaviour> vec3_tripler(new Behaviour(1));
vec3_tripler->execute = [](std::unique_ptr<std::vector<std::shared_ptr<AbstractNode>>>& a) { glm::vec3 ret = *((glm::vec3*)a->at(0)->resolve()) * 3.0f; return (void*)&ret; };

std::shared_ptr<NonterminalWrapper> ntw1(new NonterminalWrapper(vec3_doubler, { std::make_shared<Terminal<glm::vec3>>(glm::vec3(1)) }));
std::shared_ptr<NonterminalWrapper> ntw2(new NonterminalWrapper(vec3_tripler, { ntw1 }));
std::cout << *((glm::vec3*)ntw2->resolve()) << std::endl;

------------

The above code is the equivalent of the program:
std::cout << glm::vec3(1) * 2.0f * 3.0f << std::endl;
*/
class AbstractNodeWrapper : public AbstractNode{
public:

	virtual void set_parent(std::shared_ptr<AbstractNode> parent) = 0;
	// The apply function pointer is set to the behaviour of the instance at runtime. Correct usage - somenode->apply  = [&](Node* a) { int ret = (int)a->vec.at(0)->apply(a->vec.at(0)) * 2; return (void*)ret; };
	virtual std::shared_ptr<void> resolve(std::shared_ptr<std::vector<std::shared_ptr<AbstractDatum>>> d) = 0;

	// A handle into the visitor design pattern.
	virtual void accept(std::shared_ptr<AbstractVisitor> v) = 0;
};

#endif