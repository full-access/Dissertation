#ifndef RHHSYNTHESIZER_H
#define RHHSYNTHESIZER_H

#include "AbstractTreeSynthesizer.h"
#include "AbstractTypeVisitor.hpp"
#include "GPTerminalWrapper.hpp"
#include "GPNonterminalWrapper.hpp"
#include "StochasticMath.hpp"
#include "ProgramHandle.hpp"
#include "NodeLogger.hpp"

class RHHSynthesizer : public AbstractTypeVisitor, public AbstractTreeSynthesizer, public std::enable_shared_from_this<RHHSynthesizer>{
private:
	bool attach_handle;
	std::shared_ptr<NodeLogger> nl;
	int island_id;
public:
	RHHSynthesizer(int island_id) { 
		this->attach_handle = true; 
		nl = std::make_shared<NodeLogger>();
		this->island_id = island_id;
	}
	virtual std::shared_ptr<AbstractNodeWrapper> synthesize(int depth, std::shared_ptr<AbstractType> type) override {
		return nullptr;
	}
	std::shared_ptr<void> force_accept(std::shared_ptr<AbstractType> a, int depth, bool grow, bool attach_handle) {
		this->attach_handle = attach_handle;
		std::shared_ptr<void> ret = a->accept(shared_from_this(), depth, grow);
		return ret;
	}

	template <typename T>
	std::shared_ptr<void> create_node(std::shared_ptr<Type<T>> type, int depth, bool grow) {

		int _type = -1;
		if (std::is_same<T, float>::value) _type = 0;	// TEMPORARY
		if (std::is_same<T, glm::vec3>::value) _type = 1;

		bool attach = attach_handle;
		attach_handle = false;

		int n_behaviours = Register<T>::getinstance()->behaviours->size();
		int n_constants = Register<T>::getinstance()->constants->size();
		int n_arguments = Register<T>::getinstance()->arguments->size();
		std::tuple<int, int> selection;

		if (depth <= 0) {
			selection = StochasticMath().select_node<T>(0, n_constants, n_arguments, island_id);
			if (std::get<0>(selection) == 0) {
				std::cout << "breakpoint";
			}
		}
		else {
			if (grow || n_behaviours == 0)
				selection = StochasticMath().select_node<T>(n_behaviours, n_constants, n_arguments, island_id);
			else
				selection = StochasticMath().select_node<T>(n_behaviours, 0, 0, island_id);
		}

		// if our selector chose a behaviour
		if (std::get<0>(selection) == 0) {
			if (depth == 0) {
				std::cout << "breakpoint";
			}
			std::shared_ptr<GPBehaviourWrapper<T>> t_ret = Register<T>::getinstance()->behaviours->at(std::get<1>(selection));
			std::vector<std::shared_ptr<AbstractType>> typelist = t_ret->paramtypes;
			std::shared_ptr<std::vector<std::shared_ptr<AbstractNode>>> list = std::make_shared<std::vector<std::shared_ptr<AbstractNode>>>();
			for (int i = 0; i < typelist.size(); i++) {
				std::shared_ptr<AbstractNode> node = std::static_pointer_cast<AbstractNode>(force_accept(typelist.at(i), depth - 1, grow, false));
				list->push_back(node);
			}
			std::shared_ptr<GPNonterminalWrapper> ret = std::make_shared<GPNonterminalWrapper>(t_ret, list);
			for (int i = 0; i < list->size(); i++) {
				list->at(i)->set_parent(ret);
				list->at(i)->set_index(i);
			}
			if (attach) {
				std::shared_ptr<ProgramHandle> prog = std::make_shared<ProgramHandle>(ret);
				ret->set_parent(prog);
				prog->type = _type;
				prog->accept(nl);
				prog->selset = nl->get_and_delete();
				return std::static_pointer_cast<void>(prog);
			}
			ret->type = _type;
			return std::static_pointer_cast<void>(ret);
		}

		// if our selector chose a constant
		if (std::get<0>(selection) == 1) {
			std::shared_ptr<T> t_ret = Register<T>::getinstance()->constants->at(std::get<1>(selection));
			std::shared_ptr<GPTerminalWrapper<T>> ret = std::make_shared<GPTerminalWrapper<T>>(Register<T>::getinstance()->type, *t_ret);
			ret->type = Register<T>::getinstance()->type;
			if (attach) {
				std::shared_ptr<ProgramHandle> prog = std::make_shared<ProgramHandle>(ret);
				ret->set_parent(prog);
				prog->type = _type;
				prog->accept(nl);
				prog->selset = nl->get_and_delete();
				return std::static_pointer_cast<void>(prog);
			}
			ret->type = _type;
			return std::static_pointer_cast<void>(ret);
		}

		// if our selector chose an argument
		if (std::get<0>(selection) == 2) {
			std::shared_ptr<GPTerminalWrapper<T>> ret = std::make_shared<GPTerminalWrapper<T>>(Register<T>::getinstance()->type, "argv", Register<T>::getinstance()->arguments->at(std::get<1>(selection)).second);
			ret->type = Register<T>::getinstance()->type;
			if (attach) {
				std::shared_ptr<ProgramHandle> prog = std::make_shared<ProgramHandle>(ret);
				ret->set_parent(prog);
				prog->type = _type;
				prog->accept(nl);
				prog->selset = nl->get_and_delete();
				return std::static_pointer_cast<void>(prog);
			}
			ret->type = _type;
			return std::static_pointer_cast<void>(ret);
		}
	}

	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<float>> type, int depth, bool grow) override {
		return create_node<float>(type, depth, grow);
	}

	virtual std::shared_ptr<void> visit(std::shared_ptr<Type<glm::vec3>> type, int depth, bool grow) override {
		return create_node<glm::vec3>(type, depth, grow);
	}
};

#endif